<?php

/**
 * Description of Mailer
 *
 * @author Tsungai
 *
 * @updates DIRK
 *
 * @todo: limit access??
 */

define('SETTINGS', 'hp_settings');

include_once 'common.php';

write_log("[MAILER] Received request to send mail\n");

if (!isset($_POST['email_to']) ||
	!isset($_POST['email_subject']) ||
	!isset($_POST['email_content'])) {
	write_log("[MAILER] Invalid data supplied on post\n");
    die();
}

// sanitize!!!
$_POST['email_to']      = filter_var($_POST['email_to'], FILTER_SANITIZE_STRING);
$_POST['email_subject'] = filter_var($_POST['email_subject'], FILTER_SANITIZE_STRING);
$_POST['email_content'] = filter_var($_POST['email_content'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

write_log("[MAILER] email_to = " . $_POST['email_to'] . "\n");
write_log("[MAILER] email_subject = " . $_POST['email_subject'] . "\n");
write_log("[MAILER] email_content = " . $_POST['email_content'] . "\n");

if (!isset($_POST['email_from'])) {
    $_POST['email_from'] = $GLOBALS[SETTINGS]['EmailFrom'];
} else {
    $_POST['email_from'] = filter_var($_POST['email_from'], FILTER_SANITIZE_EMAIL);
}

// get the PHPMailers here
require_once 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

$mail->SMTPDebug = 3;                                           // Enable verbose debug output

$mail->isSendmail();                                            // Set mailer to use Sendmail

$mail->setFrom($_POST['email_from'], 'HelloPaisa');
// make loop for to addresses
if(isset($_POST['email_to'])) {
    // check for multiple recipients
    if(strpos($_POST['email_to'], ',') > -1) {
        $_POST['email_to'] = str_replace(' ', '', $_POST['email_to']);
        $recipients = explode(",", $_POST['email_to']);

        if (count($recipients) > 0) {
            foreach ($recipients as $recipient) {
                $mail->addAddress($recipient);
            }
        }
    } else {
        $mail->addAddress($_POST['email_to']);       // Add a recipient
    }
} else {
    write_log("[MAILER] No recipients!\n");
    die();
}
//$mail->addAddress('ellen@example.com');                 // Name is optional

$mail->addReplyTo($_POST['email_from'], 'HelloPaisa');
//$mail->addCC('cc@example.com');
//$mail->addBCC('bcc@example.com');

// loop for attachments
if ($_POST['email_attachment_names'] != null)
{
    $postData = $_POST['email_attachment_names'];

    write_log("[MAILER] FILE THINGS: ". print_r($postData, true));

    $attachments = explode('|||', $_POST['email_attachment_data']);
    $attNames = explode(',', $_POST['email_attachment_names']);

    write_log("[MAILER] NUMBER OF ATTACHMENTS = " . count($attachments));
    if(count($attachments) > 0) {
        foreach ($attachments as $key => $file_data) {
            // get filename only
            $fileName = end(explode('/', $attNames[$key]));

//            file_put_contents('/tmp/' .$fileName, base64_decode($file_data));
//
//            $mail->addAttachment('/tmp/' .$fileName);                        // Add attachments

            $mail->addStringAttachment(base64_decode($file_data), $fileName);
        }
    }
}
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');      // Optional name
//$mail->isHTML(true);                                    // Set email format to HTML

$mail->Subject = $_POST['email_subject'];
$mail->Body    = $_POST['email_content'];
//$mail->AltBody = $_POST['email_content'];

if(!$mail->send()) {
    write_log("[MAILER] Error sending the mail\n");
    write_log('[MAILER] Mailer Error: ' . $mail->ErrorInfo ."\n");
} else {
    write_log("[MAILER] Email sent successfully\n");
}
