<?php

include_once 'config.php';
include_once 'Response.php';
include_once 'async_post.php';
include_once 'TransactionChannel.php';
include_once 'PaymentProcessorResponse.php';
include_once 'SMSType.php';
include_once 'RegistrationType.php';
include_once 'vendor/autoload.php';

use Respect\Validation\Validator as v;

function get_db_connection($db_name) {

	if ($GLOBALS[$db_name] == null) {
		
		write_log("[COMMON] Connecting $db_name...\n");
		
		if ($db_name == 'HelloFin' || $db_name == 'PayAT' || $db_name == 'Partners')
		{
			$GLOBALS[$db_name] = mysqli_connect($GLOBALS[SETTINGS]['DBHostHelloPaisa'], $GLOBALS[SETTINGS]['DBUser'], $GLOBALS[SETTINGS]['DBPasswordHelloPaisa'], $db_name, $GLOBALS[SETTINGS]['DBPort']);
		}
		else
		{
			$GLOBALS[$db_name] = mysqli_connect($GLOBALS[SETTINGS]['DBHost'], $GLOBALS[SETTINGS]['DBUser'], $GLOBALS[SETTINGS]['DBPassword'], $db_name, $GLOBALS[SETTINGS]['DBPort']);
		}
	}

	if (mysqli_connect_errno()) {
		write_log("[COMMON] Failed to connect to MySQL: " . mysqli_connect_error() . "\n");
		return null;
	}

	return $GLOBALS[$db_name];
}



function is_null_or_empty($string) {
	return is_null($string) || $string == "";
}

function is_valid_msisdn($msisdn, $allow_country_code = true) {
	return v::string()->noWhitespace()->notEmpty()
			->length(10, $allow_country_code ? 9 + strlen($GLOBALS[SETTINGS]['CountryCodePrefix']) : 10)
			->validate("$msisdn");
}

function is_valid_date($date, $format = 'Y-m-d H:i:s') {
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}

function is_valid_msisdn_beneficiary($msisdn) {
	return v::string()->noWhitespace()->notEmpty()->length(2, 18)->validate("$msisdn");
}

function is_valid_source($source) {
	return v::alnum("-")->notEmpty()->length(2, 30)->validate("$source");
}

function is_valid_msisdn_customer($msisdn) {
	return v::string()->noWhitespace()->notEmpty()->length(2, 15)->validate("$msisdn");
}

function is_valid_otp($otp) {
	return v::numeric()->positive()->noWhitespace()->notEmpty()->length(5, 5)->validate("$otp");
}

function is_valid_action($action) {
	return v::string()->notEmpty()->alnum("_")->length(1, 30)->noWhitespace()->validate("$action");
}

function is_valid_xml_string($xml_string)
{
	$xml_reader = new XMLReader();
	$xml_reader->XML($xml_string);
	$xml_reader->setParserProperty(XMLReader::VALIDATE, true);
	return $xml_reader->isValid();
}

function is_valid_auth_user_id($auth_user_id) {
	return v::numeric()->positive()->noWhitespace()->length(1, 5)->notEmpty()->validate("$auth_user_id");
}

function is_valid_auth_token($auth_token) {
	return v::string()->notEmpty()->alnum()->length(1, 40)->noWhitespace()->validate("$auth_token");
}

function is_valid_imsi($imsi) {
	return v::numeric()->notEmpty()->noWhitespace()->length(1, 40)->validate("$imsi");
}

function is_valid_imei($imei) {
	return v::numeric()->notEmpty()->noWhitespace()->length(1, 40)->validate("$imei");
}

function is_valid_channel($channel) {
	return v::string()->uppercase()->alpha("_")->length(1, 40)->noWhitespace()->notEmpty()->validate("$channel");
}

function is_valid_device_transaction_id($dev_transaction_id) {
	return v::string()->notEmpty()->alnum("-_")->length(1, 40)->noWhitespace()->validate("$dev_transaction_id");
}

function is_valid_recipient($recipient) {
	return v::string()->notEmpty()->alnum("-_")->length(1, 40)->noWhitespace()->validate("$recipient");
}

function is_valid_reconciled($recon) {
    return is_numeric("$recon") && $recon >= 110 && $recon <= 116;
}


function is_valid_product_id($product_id) {
	return v::numeric()->positive()->noWhitespace()->length(1, 3)->notEmpty()->validate("$product_id");
}

function is_valid_quantity($quantity) {
	return v::numeric()->positive()->noWhitespace()->length(1, 5)->notEmpty()->validate("$quantity");
}

function is_valid_country_code($country_code) {
	return v::string()->notEmpty()->noWhitespace()->alpha()->length(1, 3)->validate("$country_code");
}

function is_valid_currency_code($currency_code) {
	return v::string()->notEmpty()->noWhitespace()->alpha()->length(3)->validate("$currency_code");
}

function is_valid_amount($amount) {
	return v::numeric()->positive()->noWhitespace()->length(1, 50)->notEmpty()->validate("$amount");
}

function is_valid_username($username) {
	return v::string()->notEmpty()->alnum("_")->noWhitespace()->length(1, 30)->validate("$username");
}

function is_valid_password($password) {
	return v::string()->notEmpty()->length(1, 30)->validate("$password");
}

function is_valid_pin($pin) {
	return v::numeric()->positive()->length(4, 5)->noWhitespace()->notEmpty()->validate("$pin");
}

function is_valid_meterNumber($meterNum) {
	return v::string()->notEmpty()->noWhitespace()->validate("$meterNum");
}

function is_valid_print_batch_id($print_batch_id) {
	return v::numeric()->positive()->notEmpty()->noWhitespace()->validate("$print_batch_id");
}

function is_valid_ref_number($ref_number) {
	return v::numeric()->positive()->noWhitespace()->length(6, 20)->notEmpty()->validate("$ref_number");
}

function is_valid_groupId($groupId) {
	return v::numeric()->positive()->noWhitespace()->length(1, 5)->notEmpty()->validate("$groupId");
}

function is_valid_hp_id($hp_id) {
	return v::alnum()->noWhitespace()->length(3, 10)->notEmpty()->validate("$hp_id");
}

function is_valid_account_number($account_num) {
	return v::numeric()->notEmpty()->length(1, 30)->validate("$account_num");
}

function is_valid_serial($voucher_serial, $length) {
	return v::string()->noWhitespace()->notEmpty()->length($length, $length)->validate("$voucher_serial");
}

function is_valid_voucher_pin($voucher_pin, $length) {
	return v::numeric()->positive()->noWhitespace()->notEmpty()->length($length, $length)->validate("$voucher_pin");
}

function is_valid_ussd_code($ussd_code) {
	return preg_match('/([^\*]{1}[0-9]+)+\#$/', $ussd_code);
}

function is_valid_ussd_msg($msg) {
	/* a to z */
	/* A to Z */
	/* 0 to 9 */
	/* underscore & dash */
	/* allowed special characters: @ # $% ^ & * () . */
	return preg_match("/^[A-Z a-z0-9_\-\@#\$%\^&.*\(\)]+$/", $msg);
}

function is_valid_ussd_session_id($session_id) {
	return v::string()->notEmpty()->length(1, 50)->validate("$session_id");
}

function is_valid_ussd_bindname($bindname) {
	return v::string()->notEmpty()->length(1, 50)->validate("$bindname");
}

function is_valid_idnumber($second) {
	return v::string()->noWhitespace()->length(2, 20)->notEmpty()->validate("$second");
}

function is_valid_title($second) {
	return v::alpha()->positive()->noWhitespace()->length(2, 10)->notEmpty()->validate("$second");
}

function is_valid_firstname($second) {
	return v::alpha(" ")->length(1, 50)->notEmpty()->validate("$second");
}

function is_valid_surname($second) {
	return v::alpha(" ")->length(2, 35)->notEmpty()->validate("$second");
}

function is_valid_dob($second) {
	return v::string()->positive()->noWhitespace()->length(10, 10)->notEmpty()->validate("$second");
}

function is_valid_streetname($second) {
	return v::string()->length(2, 70)->notEmpty()->validate("$second");
}

function is_valid_province($second) {
	$second = strtoupper($second);

	if ($second != "GAUTENG" && $second != "LIMPOPO" && $second != "NORTH WEST" && $second != "WESTERN CAPE" && $second != "EASTERN CAPE" && $second != "NORTHERN CAPE" && $second != "FREE STATE" && $second != "MPUMALANGA" && $second != "KWAZULU NATAL")
		return false;
	else
		return v::string()->length(2, 35)->notEmpty()->validate("$second");
}

function is_valid_city($second) {
	return v::string()->length(2, 35)->notEmpty()->validate("$second");
}

function is_valid_suburb($second) {
	return v::string()->length(2, 35)->notEmpty()->validate("$second");
}

function is_valid_postalcode($second) {
	return v::numeric()->positive()->noWhitespace()->length(2, 10)->notEmpty()->validate("$second");
}

function is_valid_gender($second) {

	$second = strtoupper($second);

	if ($second != "FEMALE" && $second != "MALE" && $second != "F" && $second != "M")
		return false;
	else
		return true;
}

function is_valid_foreignidnumber($second) {
	return v::string()->noWhitespace()->length(2, 20)->notEmpty()->validate("$second");
}

function is_valid_permitnumber($second) {
	return v::string()->noWhitespace()->length(2, 20)->notEmpty()->validate("$second");
}

function is_valid_streetno($second) {
    
        if(v::string()->length(1, 70)->notEmpty()->validate("$second"))
        {
            if(strlen($second) == 1)
            {
                $second = str_pad($second, 2, '0', STR_PAD_LEFT);
            }
        }    
        
	return v::string()->length(2, 70)->notEmpty()->validate("$second");
}

function is_valid_reportedlocalamount($first) {
	if ($first == 0)
		return false;
	else
		return v::numeric()->noWhitespace()->notEmpty()->validate("$first");
}

function is_valid_localcurrency($first) {
	return v::string()->noWhitespace()->length(3, 3)->notEmpty()->validate("$first");
}

function is_valid_foreignamount($first) {
	if ($first == 0)
		return false;
	else
		return v::numeric()->noWhitespace()->notEmpty()->validate("$first");
}

function is_valid_foreigncurrency($first) {
	return v::string()->noWhitespace()->length(3, 3)->notEmpty()->validate("$first");
}

function is_valid_refnumber($first) {
	return v::numeric()->positive()->noWhitespace()->length(1, 30)->notEmpty()->validate("$first");
}

function is_valid_transactiondate($first) {
	return v::string()->length(10, 20)->notEmpty()->validate("$first");
}


function is_valid_sarbreported($sarb) {
    return is_numeric("$sarb") && $sarb >= 100 && $sarb <= 109;
}


function is_valid_bopcode($first) {

	if ($first != "300" && $first != "305" && $first != "416" && $first != "400" && $first != "401")
		return false;
	else
		return v::numeric()->positive()->noWhitespace()->length(3, 11)->notEmpty()->validate("$first");
}

function is_valid_localamount($first) {
	return v::numeric()->noWhitespace()->notEmpty()->validate("$first");
}

function is_valid_totalvalue($first) {
	if ($first == 0)
		return false;
	else
		return v::numeric()->noWhitespace()->notEmpty()->validate("$first");
}

function is_valid_beneficiaryid($first) { /* return v::numeric()->positive()->noWhitespace()->length(1,20)->notEmpty()->validate("$first"); */
	return v::string()->validate("$first");
}

function is_valid_partnerstatus($first) {
	return v::numeric()->positive()->noWhitespace()->notEmpty()->validate("$first");
}

function is_valid_partnername($first) {
	return v::string()->length(1, 50)->notEmpty()->validate("$first");
}

function is_valid_country($first) {
	return v::string()->noWhitespace()->notEmpty()->validate("$first");
}

function is_valid_countrycode($first) {
	return v::string()->noWhitespace()->length(2, 2)->notEmpty()->validate("$first");
}

function is_valid_type($first) {
	return v::numeric()->noWhitespace()->length(1, 1)->notEmpty()->validate("$first");
}

function is_valid_accountno($first) {
	return v::numeric()->noWhitespace()->length(2, 40)->notEmpty()->validate("$first");
}

function is_valid_accountnumber($first) {
	return v::string()->noWhitespace()->length(2, 40)->notEmpty()->validate("$first");
}

function is_valid_idtype($first) {
	return v::string()->noWhitespace()->length(1, 1)->notEmpty()->validate("$first");
}

function is_valid_nationality($first) {
	return v::string()->noWhitespace()->length(3, 3)->notEmpty()->validate("$first");
}

function is_valid_hpid($first) {
	return v::string()->noWhitespace()->notEmpty()->validate("$first");
}

function is_valid_linkedtoref($first) {
	if ($first != "")
		return v::numeric()->positive()->noWhitespace()->length(1, 30)->notEmpty()->validate("$first");
	else
		return true;
}

function is_valid_reversalseq($first) {
	return v::numeric()->positive()->noWhitespace()->length(1, 3)->notEmpty()->validate("$first");
}

function is_msisdn_blacklisted($msisdn) {
	$sql_conn = get_db_connection('ussd_master');
	$msisdn = mysqli_real_escape_string($sql_conn, fix_msisdn($msisdn));

	$blacklist_check = mysqli_query($sql_conn, "SELECT * FROM ussd_blacklist WHERE msisdn = '$msisdn'");

	if (!$blacklist_check) {
		write_log('Error checking if MSISDN is blaklizted.');
		write_log('ERRNO: ' . mysqli_errno($sql_conn));
		write_log('ERROR: ' . mysqli_error($sql_conn));
		return false;
	} else if ($blacklist_check->num_rows > 0) {
		write_log("[COMMON] MSISDN $msisdn is blaklisted!\n");
		return true;
	} else {
		write_log("[COMMON] MSISDN $msisdn is not blaklisted\n");
		return false;
	}
}

function fix_msisdn($msisdn) {
	$pattern = "/^0/";  // Regex 
	$replacement = $GLOBALS[SETTINGS]['CountryCodePrefix'];  // Replacement string
	return preg_replace($pattern, $replacement, $msisdn, 1);
}

function get_zero_msisdn($msisdn) {
	$search = $GLOBALS[SETTINGS]['CountryCodePrefix'];
	$pattern = "/^$search/";  // Regex 
	$replacement = '0';  // Replacement string
	return preg_replace($pattern, $replacement, $msisdn, 1);
}

function remove_country_code_prefix($msisdn, $country_code_prefix) {
	$pattern = "/^$country_code_prefix/";  // Regex 
	$replacement = '0';  // Replacement string
	return preg_replace($pattern, $replacement, $msisdn, 1);
}

function formatCurrency($amount) {
	if (v::numeric()->notEmpty()->validate("$amount")) {
		return $GLOBALS[SETTINGS]['CurrencySymbol'] . number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", "$amount")), 2);
	} else {
		return $amount;
	}
}

function formatForeignCurrency($amount, $current_symbol) {
	if (v::numeric()->notEmpty()->validate("$amount")) {
		return $current_symbol . number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", "$amount")), 2);
	} else {
		return $amount;
	}
}

function alignStringToLength($string, $length, $padding) {
	while (strlen($string) < $length) {
		$string = $padding . $string;
	}
	return $string;
}

function startsWith($haystack, $needle) {
	return $needle === "" || strpos($haystack, $needle) === 0;
}

function endsWith($haystack, $needle) {
	return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

function contains($needle, $haystack)
{
    return strpos($haystack, $needle) !== false;
}

function write_log($message) {
	if ($GLOBALS['log_file'] == null) {
		$GLOBALS['log_file'] = fopen($GLOBALS[SETTINGS]['LogFile'], 'a');
	}

	//hide all passwords from the log file

	$search = "/<password>(.*?)<\/password>/";
	$replace = "<password>******</password>";

	$message = preg_replace($search, $replace, $message);

	$search = "/<Password>(.*?)<\/Password>/";
	$replace = "<Password>******</Password>";

	$message = preg_replace($search, $replace, $message);
	
	$search1 = "/<voucher_pin>(.*?)<\/voucher_pin>/";
	$replace1 = "<voucher_pin>******</voucher_pin>";

	$message = preg_replace($search1, $replace1, $message);

	$search2 = "/<auth_token>(.*?)<\/auth_token>/";
	$replace2 = "<auth_token>******</auth_token>";

	$message = preg_replace($search2, $replace2, $message);

	if (!endsWith($message, "\n")) {
		$message = "$message\n";
	}

	$message = "[" . date('Y-m-d H:i:s') . "] " . $message;

	fwrite($GLOBALS['log_file'], $message, strlen($message));
}

function db_error_log($transaction_ref_num, $script_error, $error) {
	$sql_conn = get_db_connection('TSEVD');

	$transaction_ref_num = mysqli_real_escape_string($sql_conn, $transaction_ref_num);
	$script_error = mysqli_real_escape_string($sql_conn, $script_error);
	$error = mysqli_real_escape_string($sql_conn, $error);

	mysqli_query($sql_conn, "INSERT INTO va_database_errors_log "
		. "(TransactionRefNum,ScriptError,DatabaseError,Plaform,DateTime) VALUES "
		. "($transaction_ref_num,'$script_error','$error','VoucherAPI',sysdate())");
}

function log_authentication($auth_user_id, $authentication_type, $channel, $status) {
	write_log("[COMMON] Logging authentication type $authentication_type on channel $channel for auth_user $auth_user_id. Status = $status");
	$sql_conn = get_db_connection('TSEVD');

	$status_code = $status;
	if (!is_numeric("$status")) {
		$status_code = ResponseCode::getStatusCode($status);
	}

	$auth_user_id = mysqli_real_escape_string($sql_conn, $auth_user_id);
	$channel = mysqli_real_escape_string($sql_conn, $channel);

	$authentication_result = mysqli_query($sql_conn, "INSERT INTO authentication (AuthUserID,AuthenticationType,AuthenticationTime,Channel,Status) VALUES ('$auth_user_id','$authentication_type',sysdate(),'$channel','$status_code')");

	if (!$authentication_result) {
		write_log("[COMMON] Error inserting authentication log for $auth_user_id on channel $channel."
			. " ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error inserting authentication log for $auth_user_id on channel $channel", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
	}
}

function formatXMLString($xml_string) {
	$dom = new DOMDocument;
	$dom->preserveWhiteSpace = FALSE;
	$dom->loadXML($xml_string);
	$dom->formatOutput = TRUE;
	return $dom->saveXml();
}

function formatJSONString($json_string) {
	$result = '';
	$level = 0;
	$in_quotes = false;
	$in_escape = false;
	$ends_line_level = NULL;
	$json_length = strlen($json_string);

	for ($i = 0; $i < $json_length; $i++) {
		$char = $json_string[$i];
		$new_line_level = NULL;
		$post = "";
		if ($ends_line_level !== NULL) {
			$new_line_level = $ends_line_level;
			$ends_line_level = NULL;
		}
		if ($in_escape) {
			$in_escape = false;
		} else if ($char === '"') {
			$in_quotes = !$in_quotes;
		} else if (!$in_quotes) {
			switch ($char) {
				case '}': case ']':
					$level--;
					$ends_line_level = NULL;
					$new_line_level = $level;
					break;

				case '{': case '[':
					$level++;
				case ',':
					$ends_line_level = $level;
					break;

				case ':':
					$post = " ";
					break;

				case " ": case "\t": case "\n": case "\r":
					$char = "";
					$ends_line_level = $new_line_level;
					$new_line_level = NULL;
					break;
			}
		} else if ($char === '\\') {
			$in_escape = true;
		}
		if ($new_line_level !== NULL) {
			$result .= "\n" . str_repeat("\t", $new_line_level);
		}
		$result .= $char . $post;
	}

	return $result;
}

function get_qagent_id($auth_user_id) {
	write_log("[COMMON] Getting QAgentID for AuthUserID $auth_user_id\n");
	$sql_conn = get_db_connection('TSEVD');
	$auth_user_id = mysqli_real_escape_string($sql_conn, $auth_user_id);
	$qagent_result = mysqli_query($sql_conn, "SELECT QAgentID FROM auth_user WHERE AuthUserID = '$auth_user_id'");

	if (!$qagent_result) {
		write_log("[COMMON] Error getting QAgentID for AuthUserID $auth_user_id. ERRNO:"
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error getting QAgentID for AuthUserID $auth_user_id", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return null;
	} else if ($qagent_result->num_rows == 1) {
		$row = mysqli_fetch_row($qagent_result);
		mysqli_free_result($qagent_result);
		write_log("[COMMON] Got QAgentId: " . $row[0] . "\n");
		return $row[0];
	} else {
		return null;
	}
}

function validate_otp($username, $imei, $otp, $channel) {
	write_log("[COMMON] Validating OTP for IMEI $imei & IMSI $username\n");

	$sql_conn = get_db_connection('TSVAS');

	$imei = mysqli_real_escape_string($sql_conn, $imei);
	$username = mysqli_real_escape_string($sql_conn, $username);
	$otp = mysqli_real_escape_string($sql_conn, $otp);
	$channel = mysqli_real_escape_string($sql_conn, $channel);

	$auth_user_result = mysqli_query($sql_conn, "SELECT AuthUserID FROM TSEVD.auth_user"
		. " WHERE IMEI = '$imei' "
		. " AND Username = '$username' "
		. " AND OTP = '$otp'"
		. " AND Channel = '$channel'"
		. " AND Active = 0");

	if (!$auth_user_result) {
		write_log("[COMMON] Error validating OTP for IMEI $imei & IMSI $username. ERRNO:"
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error validating OTP for IMEI $imei & IMSI $username", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return null;
	} else if ($auth_user_result->num_rows == 1) {
		$row = mysqli_fetch_row($auth_user_result);
		mysqli_free_result($auth_user_result);
		write_log("[COMMON] Got AuthUserID: " . $row[0] . "\n");
		return $row[0];
	} else {
		return null;
	}
}

function get_qagent_from_msisdn($msisdn) {
	write_log("[COMMON] Getting QAgent for MSISDN: $msisdn\n");
	$sql_conn = get_db_connection('TSVAS');

	$msisdn = mysqli_real_escape_string($sql_conn, $msisdn);

	$qagent_result = mysqli_query($sql_conn, "SELECT QAgentId FROM QAgent WHERE Msisdn = '$msisdn'");

	if (!$qagent_result) {
		write_log('Error getting QAgentId. ERRNO: '
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error getting QAgentId for MSISDN $msisdn", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return null;
	} else if ($qagent_result->num_rows > 0) {
		$row = mysqli_fetch_row($qagent_result);
		write_log("[COMMON] Got QAgentId: " . $row[0] . "\n");
		return $row[0];
	} else {
		return null;
	}
}

function get_qagent_from_username_and_channel($username, $channel) {
	write_log("[COMMON] Getting QAgentID for Username $username on channel $channel\n");

	$sql_conn = get_db_connection('TSEVD');

	$username = mysqli_real_escape_string($sql_conn, $username);

	$qagent_result = mysqli_query($sql_conn, "SELECT QAgentID FROM auth_user
		WHERE username = '$username'
		AND Channel = '$channel'
		AND Active = 1");

	if (!$qagent_result) {
		write_log("[COMMON] Error getting QAgentID for Username $username. ERRNO:"
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error getting QAgentID for Username $username", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return null;
	} else if ($qagent_result->num_rows == 1) {
		$row = mysqli_fetch_row($qagent_result);
		mysqli_free_result($qagent_result);
		write_log("[COMMON] Got QAgentId: " . $row[0] . "\n");
		return $row[0];
	} else {
		return null;
	}
}

function get_product_description($product_id, &$voucher_provider, &$voucher_type, &$voucher_amount, &$recharge_instructions) {
	$sql_conn = get_db_connection('TSEVD');

	$product_id = mysqli_real_escape_string($sql_conn, $product_id);

	$prod_respose = mysqli_query($sql_conn, "SELECT prd.ProviderName, pp.Value, pp.ProductType, pp.RechargeInstructions"
		. " FROM va_pinbased_product pp, va_provider prd"
		. " WHERE pp.ProductId = $product_id AND"
		. " pp.ProviderId = prd.ProviderId");

	if (!$prod_respose) {
		write_log("[COMMON] Error getting details for product id $product_id. ERRNO: "
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error getting details for product id $product_id", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return $product_id;
	} else if ($prod_respose->num_rows != 1) {
		return $product_id;
	}

	$voucher_row = mysqli_fetch_row($prod_respose);
	$voucher_provider = $voucher_row[0];
	$voucher_type = $voucher_row[2];
	$voucher_amount = $voucher_row[1];
	$recharge_instructions = $voucher_row[3];
	return "$voucher_provider " . formatCurrency($voucher_amount) . " $voucher_type";
}

function sendSMS($msg, $msisdn, $sms_type = null, $sms_reference = null, $sms_max_retries = null) {
	$params = array(
	    "sms_to" => fix_msisdn($msisdn),
	    "sms_content" => $msg,
	    "sms_settings" => SETTINGS,
	);

	if (!is_null_or_empty($sms_type))           $params['sms_type']         = $sms_type;
	if (!is_null_or_empty($sms_reference))      $params['sms_reference']    = $sms_reference;
	if (!is_null_or_empty($sms_max_retries))    $params['sms_max_retries']  = $sms_max_retries;

	curl_post_async($GLOBALS[SETTINGS]['SMSSenderLocation'], $params);
}

function sendEmail($subject, $message, $recipient = null, $attachment = null) {
	if ($recipient == null) {
		$recipient = $GLOBALS[SETTINGS]['ITSupportEmail'];
	}

	// check for and BASE64 attachments
	write_log('[SENDEMAIL] Attachment param: ' . $attachment);

	$attachmentB64 = null;
	if($attachment) {
		$attachment = str_replace(' ', '', $attachment);
		$attachmentsArr = explode(",", $attachment);

		//write_log('[SENDEMAIL] ' . print_r($attachmentsArr, true));

		if(count($attachmentsArr) > 0) {
			foreach ($attachmentsArr as $fileLocation) {
				//encode
				//write_log('[SENDEMAIL] ' . $fileLocation);
				$fileInside = file_get_contents($fileLocation);
				$attachmentB64 .= base64_encode($fileInside) .'|||';
			}
		}
	}
	$attachmentB64 = rtrim($attachmentB64, '|||');

	$params = array(
	    "email_to" => "$recipient",
	    "email_subject" => "$subject",
	    "email_content" => "$message\n",
	    "email_settings" => SETTINGS,
		"email_attachment_names" => $attachment,
	    "email_attachment_data" => $attachmentB64
	);

	write_log('[SENDEMAIL] '.$GLOBALS[SETTINGS]['MailerLocation']. '|||'. print_r($params,true));

	//curl_post_async($GLOBALS[SETTINGS]['MailerLocation'], $params);
	$ch = curl_init($GLOBALS[SETTINGS]['MailerLocation']);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_exec($ch);

	if(curl_errno($ch)) {
		write_log('[SENDEMAIL] Curl error: '. curl_error($ch));
	} else {
		write_log('[SENDEMAIL] Curl success.');
	}
	
	curl_close($ch);
}

function log_hellopaisa_sms($reference_no, $msisdn, $sms_type, $msg)
{
	$sql_conn = get_db_connection('HelloFin');
	$reference_no = is_null_or_empty($reference_no) ? 'NULL' : "'" . mysqli_real_escape_string($sql_conn, $reference_no) . "'";
	$msisdn = mysqli_real_escape_string($sql_conn, $msisdn);
	$sms_type = mysqli_real_escape_string($sql_conn, $sms_type);
	$msg = mysqli_real_escape_string($sql_conn, $msg);

	write_log("[DB_CONNECTION] Logging SMS for reference $reference_no to $msisdn. Type = $sms_type");

	$sql = "INSERT INTO HelloPaisaSMS (RefNumber,MSISDN,SMSType,Message,DateTime) VALUES ($reference_no,'$msisdn','$sms_type','$msg',sysdate())";

	$sql_result = mysqli_query($sql_conn, $sql);

	if (!$sql_result)
	{
		write_log("[DB_CONNECTION] Error logging SMS for reference $reference_no to $msisdn");
		write_log("[DB_CONNECTION] MYSQL ERRNO: " . mysqli_errno($this->conn));
		write_log("[DB_CONNECTION] MYSQL ERROR: " . mysqli_error($this->conn));
		sendEmail("Error logging SMS for reference $reference_no to $msisdn",
			"Error logging SMS for reference $reference_no to $msisdn:\r\n$sql\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($this->conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($this->conn) . "\r\n");

		return false;
	}
	else
	{
		write_log("[DB_CONNECTION] Logged new SMS with ID " . $sql_conn->insert_id);
		return true;
	}
}

function update_response($response_xml, $status) {
	return update_response_xml($response_xml, $status, ResponseCode::getStatusDescription($status));
}

function update_response_xml($response_xml, $status, $statusDescription) {
	$par = $response_xml->xpath('//payATapiResponse');

	$status = $status;
	if (!is_numeric("$status")) {
		$status = ResponseCode::getStatusCode($status);
	}

	if (!isset($par[0]->status)) {
		write_log("[COMMON] Setting transaction response: $statusDescription\n");
		$par[0]->addChild('status', $status);
		$par[0]->addChild('statusDescription', $statusDescription);
	}

	return formatXMLString($response_xml->asXML());
}

function ts_encrypt($plainText) {

	$iv = hash("sha512", $GLOBALS[SETTINGS]['InitializationVector']);
	$key_file = fopen($GLOBALS[SETTINGS]['KeyFile'], 'r');
	$key = fgets($key_file);

	$padded = pkcs5_pad($plainText, mcrypt_get_block_size("tripledes", "cbc"));

	$encText = mcrypt_encrypt("tripledes", $key, $padded, "cbc", $iv);

	return base64_encode($encText);
}

function ts_decrypt($encryptText) {
	$iv = hash("sha512", $GLOBALS[SETTINGS]['InitializationVector']);
	$key_file = fopen($GLOBALS[SETTINGS]['KeyFile'], 'r');
	$key = fgets($key_file);

	$cipherText = base64_decode($encryptText);

	$res = mcrypt_decrypt("tripledes", $key, $cipherText, "cbc", $iv);

	$resUnpadded = pkcs5_unpad($res);

	return $resUnpadded;
}

function pkcs5_pad($text, $blocksize) {
	$pad = $blocksize - (strlen($text) % $blocksize);
	return $text . str_repeat(chr($pad), $pad);
}

function pkcs5_unpad($text) {
	$pad = ord($text{strlen($text) - 1});
	if ($pad > strlen($text))
		return false;
	if (strspn($text, chr($pad), strlen($text) - $pad) != $pad)
		return false;
	return substr($text, 0, -1 * $pad);
}


function post_to_url($url, $params)
{
	write_log("[COMMON] Posting to $url:\nParams:\t\t$params");
    	
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
	
    if (curl_errno($ch)) 
    {
        write_log("[COMMON] Failed to post to URL! " . curl_errno($ch) . " MSG: " . curl_error($ch) . "\n");
        return curl_error($ch);
    } 
    else 
    {
		write_log("[COMMON] Got response\n$output\n");
        curl_close($ch);
        return $output;
    }
}

function get_auid_from_username_and_channel($username, $channel) {
	write_log("[COMMON] Getting AuthUserID for USERNAME: $username\n");
	$sql_conn = get_db_connection('TSEVD');

	$username = mysqli_real_escape_string($sql_conn, $username);

	$qagent_result = mysqli_query($sql_conn, "SELECT AuthUserId FROM auth_user"
		. " WHERE Username = '$username'"
		. " AND Channel = '$channel'");

	if (!$qagent_result) {
		write_log('Error getting AuthUserID. ERRNO: '
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error getting AuthUserID for USERNAME $username", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return null;
	} else if ($qagent_result->num_rows == 1) {
		$row = mysqli_fetch_row($qagent_result);
		write_log("[COMMON] Got AuthUserId: " . $row[0] . "\n");
		return $row[0];
	} else {
		return null;
	}
}

function get_auid_from_termid_and_username($terminal_id, $username) {
	write_log("[COMMON] Getting AuthUserID for IMEI: $terminal_id, StoreID: $username\n");
	$sql_conn = get_db_connection('TSEVD');

	$terminal_id = mysqli_real_escape_string($sql_conn, $terminal_id);
	$username = mysqli_real_escape_string($sql_conn, $username);

	$qagent_result = mysqli_query($sql_conn, "SELECT AuthUserId FROM auth_user"
		. " WHERE IMEI = '$terminal_id'"
		. " AND Username = '$username'"
		. " AND Channel = '" . TransactionChannel::POS . "'"
		. " AND Active = 1");

	if (!$qagent_result) {
		write_log('Error getting AuthUserID. ERRNO: '
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error getting AuthUserID for IMEI $terminal_id Username: $username", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return null;
	} else if ($qagent_result->num_rows > 0) {
		$row = mysqli_fetch_row($qagent_result);
		write_log("[COMMON] Got AuthUserId: " . $row[0] . "\n");
		return $row[0];
	} else {
		return null;
	}
}

function get_auid_from_imei_and_username($imei, $username) {
	write_log("[COMMON] Getting AuthUserID for IMEI: $imei, USERNAME: $username\n");
	$sql_conn = get_db_connection('TSEVD');

	$imei = mysqli_real_escape_string($sql_conn, $imei);
	$username = mysqli_real_escape_string($sql_conn, $username);

	$qagent_result = mysqli_query($sql_conn, "SELECT AuthUserId FROM auth_user"
		. " WHERE IMEI = '$imei'"
		. " AND Username = '$username'"
		. " AND Channel = '" . TransactionChannel::SMARTPHONE . "'"
		. " AND Active = 1");

	if (!$qagent_result) {
		write_log('Error getting AuthUserID. ERRNO: '
			. mysqli_errno($sql_conn) . " MSG: "
			. mysqli_error($sql_conn) . "\n");
		db_error_log('null', "Error getting AuthUserID for IMEI $imei USERNAME: $username", "ERRNO: " . mysqli_errno($sql_conn)
			. " MSG: " . mysqli_error($sql_conn));
		return null;
	} else if ($qagent_result->num_rows > 0) {
		$row = mysqli_fetch_row($qagent_result);
		write_log("[COMMON] Got AuthUserId: " . $row[0] . "\n");
		return $row[0];
	} else {
		return null;
	}
}

function get_msisdn_from_hpid($hpid) {
	$sql_conn = get_db_connection('HelloFin');

	write_log("[COMMON] Getting MSISDN for customer $hpid");
	$msisdn_result = $sql_conn->query(
		"SELECT hc.Msisdn from HelloCustomer hc, HelloPaisaCustomer hpc
	     WHERE hpc.HpId='$hpid' AND hc.HgId = hpc.HgId");

	if (!$msisdn_result) {
		write_log("[COMMON] Error getting MSISDN for HelloCustomer $hpid.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Failed to get MSISDN for customer $hpid", "Failed to get MSISDN for customer $hpid:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	} else if ($msisdn_result->num_rows == 1) {
		$result = mysqli_fetch_array($msisdn_result);
		write_log("[COMMON] Got MSISDN " . $result['Msisdn'] . " for customer $hpid");
		return $result['Msisdn'];
	}

	write_log("[COMMON] Failed to get MSISDN for customer $hpid. Num rows = " . $msisdn_result->num_rows);
	sendEmail("Failed to get MSISDN for customer $hpid", "Failed to get MSISDN for customer $hpid:\r\n" .
		"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
		"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
	return null;
}

function get_balance_for_hpid($hpid) {
	$sql_conn = get_db_connection('HelloFin');
	$hpid = mysqli_real_escape_string($sql_conn, $hpid);

	write_log("[COMMON] Getting balance for customer $hpid");
	$credit_result = $sql_conn->query("SELECT IFNULL(sum(Cost),0) as 'Credit' from HelloPaisaDailyLedger where HpId = '$hpid' AND DebitCredit = 'C'");
	$debit_result = $sql_conn->query("SELECT IFNULL(sum(Cost),0) as 'Debit' from HelloPaisaDailyLedger where HpId = '$hpid' AND DebitCredit = 'D'");

	if (!$credit_result || !$debit_result) {
		write_log("[COMMON] Error getting balance for HelloCustomer $hpid.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting balance for HelloCustomer $hpid", "Error getting balance for HelloCustomer $hpid:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$cresult = mysqli_fetch_array($credit_result);
	$dresult = mysqli_fetch_array($debit_result);

	$balance = $cresult['Credit'] - $dresult['Debit'];

	write_log("[COMMON] Got available balance $balance for HelloCustomer $hpid");

	return $balance;
}

function get_outstanding_transaction_balance($hpid) {
	$sql_conn = get_db_connection('HelloFin');
	$hpid = mysqli_real_escape_string($sql_conn, $hpid);

	write_log("[COMMON] Getting outstanding transaction balance for customer $hpid");        
        
	$outstandingAmount = $sql_conn->query("SELECT IFNULL(sum(ReportedLocalAmount),0) as 'balance' from HelloPaisaTransactions where HpId = '$hpid' AND MatchStatus = '20'");

	if (!$outstandingAmount) {
		write_log("[COMMON] Error getting outstanding transactions balance for HelloCustomer $hpid.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting outstanding transactions balance for HelloCustomer $hpid", "Error getting outstanding transactions balance for HelloCustomer $hpid:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$outBalance = mysqli_fetch_array($outstandingAmount);

	write_log("[COMMON] Got outstanding balance ".$outBalance['balance']." for HelloCustomer $hpid");

	return $outBalance['balance'];
}

function get_hpid_for_msisdn($msisdn) {
	$sql_conn = get_db_connection('HelloFin');

	$msisdn = get_zero_msisdn(mysqli_real_escape_string($sql_conn, $msisdn));

	write_log("[COMMON] Getting HPID for msisdn $msisdn");
	$hpid_result = $sql_conn->query(
		"SELECT hpc.HpId from HelloCustomer hc, HelloPaisaCustomer hpc
		 WHERE hc.Msisdn = '$msisdn' AND hc.HgId = hpc.HgId
		 AND hc.StatusId = '" . HFResponseCode::getStatusCode(ResponseCode::ACTIVE) . "'");

	if (!$hpid_result) {
		write_log("[COMMON] Error getting HPID for msisdn $msisdn.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting HPID for msisdn $msisdn", "Error getting HPID for msisdn $msisdn:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	} else if ($hpid_result->num_rows == 1) {
		$result = mysqli_fetch_row($hpid_result);
		write_log("[COMMON] Got HPID for msisdn $msisdn as " . $result[0]);
		return $result[0];
	}
	write_log("[COMMON] MSISDN $msisdn is not registered as a HelloCustomer.");
	return null;
}

function get_partner_id_for_bank($bank, $country) {
	$sql_conn = get_db_connection('HelloFin');
	$bank = mysqli_real_escape_string($sql_conn, $bank);
	$country = mysqli_real_escape_string($sql_conn, $country);

	write_log("[COMMON] Getting Partner Id for $bank in $country");
	$hpid_result = $sql_conn->query(
		"SELECT hpbc.Id from HelloPaisaPartners hpbc, HelloPaisaCountries hpc
		 WHERE hpbc.PartnerName = '$bank' AND hpc.Country = '$country' AND hpc.Active = 1");

	if (!$hpid_result) {
		write_log("[COMMON] Error getting Partner Id for $bank in $country.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting Partner Id for $bank in $country", "Error getting Partner Id for $bank in $country:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$result = mysqli_fetch_row($hpid_result);

	write_log("[COMMON] Got Partner Id for $bank in $country: " . $result[0]);

	return $result[0];
}

function get_all_countries() {
	$sql_conn = get_db_connection('HelloFin');

	write_log("[COMMON] Getting Hello Paisa Countries");
	$country_result = $sql_conn->query("SELECT Country from HelloPaisaCountries WHERE Active = 1 ORDER BY DisplayPriority ASC");

	if (!$country_result) {
		write_log("[COMMON] Error getting Hello Paisa Countries.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting Hello Paisa Countries", "Error getting Hello Paisa Countries:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_row($country_result)) {
		$response[++$count] = $row[0];
	}

	write_log("[COMMON] Got $country_result->num_rows Hello Paisa Countries ");

	return $response;
}

function get_all_banks_for_country($country) {
	$sql_conn = get_db_connection('HelloFin');
	$country = mysqli_real_escape_string($sql_conn, $country);

	write_log("[COMMON] Getting partners for $country");
	$partner_result = $sql_conn->query(
		"SELECT hpb.BankName, hpb.Id as BankId, hpb.PartnerId from HelloPaisaBanks hpb, HelloPaisaPartners hpbp, HelloPaisaCountries hpc
		 WHERE hpc.Country = '$country' AND hpb.PartnerId = hpbp.Id AND hpb.CountryId = hpc.Id AND hpc.Active = 1 AND hpb.Enabled = 1");

	if (!$partner_result) {
		write_log("[COMMON] Error getting partners for $country.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting partners for $country", "Error getting partners for $country:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_array($partner_result)) {
		++$count;
		$response[$count]['BankName'] = $row['BankName'];
		$response[$count]['BankId'] = $row['BankId'];
		$response[$count]['PartnerId'] = $row['PartnerId'];
	}

	write_log("[COMMON] Got $partner_result->num_rows Hello Paisa partners for $country");

	return $response;
}

function get_all_banks_for_country_using_type($country, $type) {
	$sql_conn = get_db_connection('HelloFin');
	$country = mysqli_real_escape_string($sql_conn, $country);
	$type = mysqli_real_escape_string($sql_conn, $type);

	write_log("[COMMON] Getting partners for $country");
	$partner_result = $sql_conn->query("SELECT b.BankName, b.Id as BankId, b.PartnerId
					FROM HelloPaisaBanks b, HelloPaisaCountries c,HelloPaisaPartners p
					WHERE b.`Type` = '$type'
					AND b.CountryId = c.Id
					AND b.Enabled = 1
					AND c.Active = 1
					AND p.Id = b.PartnerId
					AND c.Country = '$country'
					Order by b.DisplayPriority");

	if (!$partner_result) {
		write_log("[COMMON] Error getting partners for $country.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting partners for $country", "Error getting partners for $country:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_array($partner_result)) {
		++$count;
		$response[$count]['BankName'] = $row['BankName'];
		$response[$count]['BankId'] = $row['BankId'];
		$response[$count]['PartnerId'] = $row['PartnerId'];
	}

	write_log("[COMMON] Got $partner_result->num_rows Hello Paisa partners for $country");

	return $response;
}

function get_all_partner_fields($bankId) {
	$sql_conn = get_db_connection('HelloFin');
	$country = mysqli_real_escape_string($sql_conn, $country);
	$bankId = mysqli_real_escape_string($sql_conn, $bankId);

	write_log("[COMMON] Getting partners fields for $bankId");
	$partner_result = $sql_conn->query("SELECT *
					FROM `HelloPaisaBankPartnersFields`
					WHERE `BankId` =$bankId");

	if (!$partner_result) {
		write_log("[COMMON] Error getting partners for $country.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting partners for $country", "Error getting partners for $country:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_array($partner_result)) {
		$response[$count]['FieldId'] = $row['FieldId'];
		$response[$count]['FieldLabel'] = $row['FieldLabel'];
		$response[$count]['Regex'] = $row['Regex'];
		$response[$count]['FieldValidationMessage'] = $row['FieldValidationMessage'];
		$response[$count]['BankingPartnerId'] = $row['BankingPartnerId'];
		$response[$count]['IsExtraTable'] = $row['IsExtraTable'];
		$count ++;
	}

	write_log("[COMMON] Got $partner_result->num_rows Hello Paisa partners for $country");

	return $response;
}

function get_all_recipients_for_hpid($hpid) {
	$sql_conn = get_db_connection('HelloFin');
	$hpid = mysqli_real_escape_string($sql_conn, $hpid);

	write_log("[COMMON] Getting recipients for HPID $hpid");

	$partner_result = $sql_conn->query("SELECT distinct (hpb.Id), CONCAT(hpb.FirstName, ' ', hpb.SurName, ' (' , ifnull(hpbank.DisplayName,''),')') as RecipientName, hpp.Id as PartnerId, hpb.BankId,hpbank.CountryId, gp.GroupID
						FROM HelloPaisaBeneficiary hpb, HelloPaisaBanks hpbank, HelloPaisaPartners hpp, HelloPaisaFX hpfx, HelloPaisaGroupProfile gp
						WHERE hpb.HpId = '$hpid'
						AND hpb.Status = '1'
						AND hpb.BankId = hpbank.Id
						AND hpbank.PartnerId = hpp.Id
						AND hpbank.CountryId = hpfx.CountryId
						AND gp.BankId = hpb.BankId
						AND hpbank.Enabled = 1 order by hpb.CreationDate desc");

	if (!$partner_result) {
		write_log("[COMMON] Error getting recipients for $hpid.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting recipients for $hpid", "Error getting recipients for $hpid:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_array($partner_result)) {
		++$count;
		$response[$count]['BeneficiaryId'] = $row['Id'];
		$response[$count]['RecipientName'] = $row['RecipientName'];
		$response[$count]['PartnerId'] = $row['PartnerId'];
		$response[$count]['BankId'] = $row['BankId'];
		$response[$count]['CountryId'] = $row['CountryId'];
		$response[$count]['GroupID'] = $row['GroupID'];
	}

	write_log("[COMMON] Got $partner_result->num_rows recipients for $hpid");
	return $response;
}

function get_currency_for_country($countryId,$bankId) {
	$sql_conn = get_db_connection('HelloFin');
	$countryId = mysqli_real_escape_string($sql_conn, $countryId);
	$bankId = mysqli_real_escape_string($sql_conn, $bankId);
	write_log("[COMMON] Getting get_currency_for_country, country id : $countryId");
	$menu_result = $sql_conn->query("SELECT distinct f.CurrencyCode
					FROM HelloPaisaFX f, HelloPaisaUnsupportedFX fx
					WHERE f.CountryId = '$countryId' and f.CurrencyCode not in 
					(select CurrencyCode from HelloPaisaUnsupportedFX where BankId='$bankId')");

	if (!$menu_result) {
		write_log("[COMMON] Error getting partners for get_all_payout_types .");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting partners for get_currency_for_country ", "Error getting partners for get_all_payout_types:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	write_log("[COMMON] Got $menu_result->num_rows Hello Paisa Currencies for $countryId");
	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_array($menu_result)) {
		$response[$count]['CurrencyCode'] = $row['CurrencyCode'];
		++$count;
	}
	return $response;
}

function get_available_partner_funds($partner_id) {
	$sql_conn = get_db_connection('HelloFin');
	$partner_id = mysqli_real_escape_string($sql_conn, $partner_id);

	write_log("[COMMON] Getting available funds for partner $partner_id");
	$available_result = $sql_conn->query(
		"SELECT IFNULL(AvailableBalance,0) - IFNULL(SUM(hpt.ForeignAmount),0)
		 FROM HelloPaisaTransactions hpt, HelloPaisaPartners hpbp, HelloPaisaBeneficiary hpb, HelloPaisaBanks hpbank
		 WHERE hpt.PartnerStatus != 5000
		 AND hpt.MatchStatus = 21
		 AND hpt.BeneficiaryId = hpb.Id
		 AND hpb.BankId = hpbank.Id
		 AND hpbank.PartnerId = '$partner_id'
		 AND hpbp.Id = hpbank.PartnerId");

	if (!$available_result) {
		write_log("[COMMON] Error getting available funds for partner $partner_id.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting available funds for partner $partner_id", "Error getting available funds for partner $partner_id:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$result = mysqli_fetch_row($available_result);
	write_log("[COMMON] Got available balance for partner as " . $result[0]);
	return $result[0];
}

function get_partner_name($partner_id) {
	$sql_conn = get_db_connection('HelloFin');
	$partner_id = mysqli_real_escape_string($sql_conn, $partner_id);

	write_log("[COMMON] Getting partner name for partner ID $partner_id");
	$name_result = $sql_conn->query("SELECT PartnerName from HelloPaisaPartners hpbp WHERE Id = '$partner_id'");

	if (!$name_result) {
		write_log("[COMMON] Error getting partner name for partner ID $partner_id.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting available funds for partner $partner_id", "Error getting partner name for partner ID $partner_id:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$result = mysqli_fetch_row($name_result);
	write_log("[COMMON] Got partner name for partner ID $partner_id as " . $result[0]);
	return $result[0];
}

function update_partner_balance($partner_id, $remaining_balance)
{
	$sql_conn = get_db_connection('HelloFin');

//	if (is_null_or_empty($partner_id) || (!is_numeric($remaining_balance) && !is_float($remaining_balance)))
//	{
//		write_log("[COMMON] Cant update balance. Invalid data Partner: $partner_id | Balance: $remaining_balance");
//		sendEmail("Error updating balance for partner $partner_id to $remaining_balance",
//				  "Error updating balance for partner $partner_id to $remaining_balance\r\n");
//		return false;
//	}

	$partner_id			= $sql_conn->real_escape_string($partner_id);
	$remaining_balance	= $sql_conn->real_escape_string($remaining_balance);

        $partnerCurrencyArr = $sql_conn->query("SELECT BalanceCurrency from HelloPaisaPartners hpbp WHERE Id = '$partner_id'");
        $partnerCurrencyRow = mysqli_fetch_array($partnerCurrencyArr);
        
        $partnerCurrency = $partnerCurrencyRow['BalanceCurrency'];
        
        if($partnerCurrency == "USD")
        {
            $balance_sql = "UPDATE HelloPaisaPartners SET USDBalance = '$remaining_balance', BalanceUpdateTime = sysdate() WHERE Id = '$partner_id'";
        }
        else
        {
            $balance_sql = "UPDATE HelloPaisaPartners SET AvailableBalance = '$remaining_balance', BalanceUpdateTime = sysdate() WHERE Id = '$partner_id'";
        }
        
	

	$balance_result = $sql_conn->query($balance_sql);

	if (!$balance_result) 
	{
		write_log("[COMMON] Error updating balance for partner $partner_id to $remaining_balance");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error updating balance for partner $partner_id to $remaining_balance",
				  "Error updating balance for partner $partner_id to $remaining_balance:\r\n$balance_sql\r\n" .
				  "MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
				  "MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");

		return false;
	}

	return true;
}

function get_unused_tran_ref() {
	write_log("[COMMON] Getting new unused reference number.");
	$ch = curl_init($GLOBALS[SETTINGS]['GetReferenceLocation']);

	curl_setopt($ch, CURLOPT_GET, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);

	if (curl_errno($ch)) {
		write_log("[COMMON] Failed to get available reference number. " . curl_error($ch));
		sendEmail("Failed to get available reference number", "Failed to get available reference number\r\n" . curl_error($ch));

		return null;
	} else {
		write_log("[COMMON] Got new reference number $output");
		curl_close($ch);
		return trim($output);
	}
}

function insert_transaction_audit($ref_number, $match_status, $partner_status, $sarb_reported, $reconciled, $bop_code, $channel, $comment, $updated_by) {
	$sql_conn = get_db_connection('HelloFin');

	$ref_number = mysqli_real_escape_string($sql_conn, $ref_number);
	$match_status = mysqli_real_escape_string($sql_conn, $match_status);
	$sarb_reported = mysqli_real_escape_string($sql_conn, $sarb_reported);
	$reconciled = mysqli_real_escape_string($sql_conn, $reconciled);
	$bop_code = mysqli_real_escape_string($sql_conn, $bop_code);
	$channel = mysqli_real_escape_string($sql_conn, $channel);
	$comment = mysqli_real_escape_string($sql_conn, $comment);
	$updated_by = mysqli_real_escape_string($sql_conn, $updated_by);

	write_log("[COMMON] Inserting transaction audit for transaction $ref_number updated by $updated_by on channel $channel.");

	$audit_sql = "INSERT INTO HelloPaisaTransactionsAudit (RefNumber,MatchStatus,PartnerStatus,SarbReported,Reconciled,BopCode,Channel,Comment,UpdatedBy,UpdatedTime)
				  VALUES ('$ref_number','$match_status','$partner_status','$sarb_reported','$reconciled','$bop_code','$channel','$comment','$updated_by', sysdate());";

	$audit_insert_result = $sql_conn->query($audit_sql);

	if (!$audit_insert_result) {
		write_log("[COMMON] Error inserting audit trail for updated transation '" . $ref_number . "'.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error inserting audit trail for updated transation $ref_number", "Error inserting audit trail for updated transation $ref_number:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");

		return false;
	}

	return true;
}

function get_all_payout_types($country) {
	$sql_conn = get_db_connection('HelloFin');

	write_log("[COMMON] Getting get_all_payout_types");
	$partner_result = $sql_conn->query("SELECT distinct(b.type) as Id, t.PartnerString, t.Description
				FROM `HelloPaisaBanks` b, `HelloPaisaPayOutTypes` t, `HelloPaisaCountries` c
				WHERE b.`Type` = t.Id 
				AND b.countryId = (Select c.id FROM HelloPaisaCountries c where c.Country = '$country')
				AND b.Enabled = 1
				AND c.Active = 1");

	if (!$partner_result) {
		write_log("[COMMON] Error getting partners for get_all_payout_types .");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting partners for get_all_payout_types", "Error getting partners for get_all_payout_types:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_array($partner_result)) {
		++$count;
		$response[$count]['Id'] = $row['Id'];
		$response[$count]['PartnerString'] = $row['PartnerString'];
		$response[$count]['Description'] = $row['Description'];
	}

	write_log("[COMMON] Got $partner_result->num_rows Hello Paisa partners for $country");

	return $response;
}

function get_electricity_reference($reprintDate, $meter_number) {
	$sql_conn = get_db_connection('TSVAS');
	$reprintDate = mysqli_real_escape_string($sql_conn, $reprintDate);
	$meter_number = mysqli_real_escape_string($sql_conn, $meter_number);

	write_log("[COMMON] Getting transaction ID From Meter number $meter_number on date $reprintDate");
	$refNumber = $sql_conn->query(
		"SELECT MAX(TransactionRefNum) FROM syntell_ledger WHERE meter_number = '$meter_number' AND date = '$reprintDate'");

	if (!$refNumber) {
		write_log("[COMMON] Error getting Transaction Reference Number From Meter number $meter_number on date $reprintDate");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting Transaction Reference Number", "Error getting Transaction Reference Number From Meter number $meter_number on date $reprintDate:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	write_log("[COMMON] Got $refNumber->num_rows Reference Number");

	$row = mysqli_fetch_row($refNumber);

	return $row[0];
}

function get_ussd_menu_items($menuId) {
	$sql_conn = get_db_connection('HelloFin');
	$menuId = mysqli_real_escape_string($sql_conn, $menuId);
	write_log("[COMMON] Getting get_ussd_menu_items, Menu ID : $menuId");
	$menu_result = $sql_conn->query("SELECT * FROM `HelloPaisaUSSDMenus` WHERE `MenuId` =$menuId");

	if (!$menu_result) {
		write_log("[COMMON] Error getting partners for get_all_payout_types .");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting partners for get_all_payout_types", "Error getting partners for get_all_payout_types:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	write_log("[COMMON] Got $menu_result->num_rows Hello Paisa menu $menuId");
	$row = mysqli_fetch_array($menu_result);
	return $row;
}

function getBankGroupId($bankId) {
	$sql_conn = get_db_connection('HelloFin');
	$bankId = mysqli_real_escape_string($sql_conn, $bankId);
	write_log("[COMMON] Getting getGroupId, Bank ID : $bankId");
	$groupGet = $sql_conn->query("SELECT `GroupID` FROM `HelloPaisaGroupProfile` where `BankId` ='$bankId'");

	if (!$groupGet) {
		write_log("[COMMON] Error getting partners for get_all_payout_types .");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting partners for get_all_payout_types", "Error getting partners for get_all_payout_types:\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}
	$groupArr = mysqli_fetch_array($groupGet);

	write_log("[COMMON] Got groupid " . $groupArr[0]);
	return $groupArr[0];
}

function get_country_id_name() {
	$sql_conn = get_db_connection('HelloFin');
	write_log("[COMMON] get_country_id_name");
	
	$partner_result = $sql_conn->query("SELECT DISTINCT (fx.`CountryId`), c.`Country` 
				FROM  `HelloPaisaFX` AS fx
				INNER JOIN  `HelloPaisaCountries` AS c ON fx.`CountryId` = c.`Id` WHERE c.Id != '2558'");

	if (!$partner_result) {
		write_log("[COMMON] Error getting Countries.");
		write_log("[COMMON] MYSQL ERRNO: " . mysqli_errno($sql_conn));
		write_log("[COMMON] MYSQL ERROR: " . mysqli_error($sql_conn));
		sendEmail("Error getting Countries and ID", "\r\n" .
			"MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			"MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
		return null;
	}

	$response = array();
	$count = 0;
	while ($row = mysqli_fetch_array($partner_result)) {
		++$count;
		$response[$count]['CountryId'] = $row['CountryId'];
		$response[$count]['Country'] = $row['Country'];
	}

	write_log("[COMMON] Got $partner_result->num_rows Countries");

	return $response;
}

function getCallCentreNumber(){
    return $GLOBALS['callcentre_settings']['call_centre_number'];
}
