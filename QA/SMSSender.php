<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mailer
 *
 * @author Tsungai
 */

if (isset($_REQUEST['sms_settings'])) {
	define('SETTINGS', $_REQUEST['sms_settings']);
} else {
	define('SETTINGS', 'hp_settings');
}

include_once 'common.php';

write_log("[SMSSender] Received request to send sms");

if (!isset($_REQUEST['sms_to']) ||
	!isset($_REQUEST['sms_content'])) {
	write_log("[SMSSender] Invalid data supplied");
	echo "Invalid data supplied";
}

if (isset($_REQUEST['sms_max_retries'])) {
	$max_retries = $_REQUEST['sms_max_retries'];
	write_log("[SMSSender] Invalid data supplied");
	echo "Invalid data supplied";
}
else {
	$max_retries = 3;
}

write_log("[SMSSender] sms_to = " . $_REQUEST['sms_to']);
write_log("[SMSSender] sms_content = " . $_REQUEST['sms_content']);
write_log("[SMSSender] sms_max_retries = " . $max_retries);

$url = $GLOBALS[SETTINGS]['SMSURL']
			. "?username=telesimtracker"
			. "&password=RHMYrWBRdm3yjSWT"
			. "&from=27840001383366"
			. "&to=" . $_REQUEST['sms_to']
			. "&text=" . str_replace(' ', '+', $_REQUEST['sms_content'])
			. "&priority=3&smsc=ccbulk";
write_log("[SMSSender] $url");

/**
 *  DIRK did this.
 *
 *  SMS Sending has been crippled on QA because of issues caused with Cell C when testing
 *
 *  To view SMS's sent, goto HelloFin.HelloPaisaSMS (for this to work you need to be sending an sms_type in your request)
 *
 *  For more info contact Dirk or Shaazim
 

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_PORT, 13013);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

$retries = $max_retries;

$accepted = FALSE;

while ($accepted === FALSE && $retries-- > 0)
{
	$output = curl_exec($ch);
	
	$accepted = strpos($output, "Accepted for delivery");
	
	if ($accepted)
	{
		write_log("[SMSSender] SMS sent successfully");
		echo "SMS sent successfully";
		break;
	}
	else if ($retries > 0)
	{
		write_log("[SMSSender] SMS submission failed. Accepted = $accepted. Curl returned: $output. Will retry $retries more times.");
		echo "SMS submission failed. Curl returned: $output. Will retry $retries more times";
		sleep(3);
	}
	else
	{
		write_log("[SMSSender] SMS submission failed after $max_retries attempts");
		echo "SMS submission failed after $max_retries attempts";
	}
}

curl_close($ch);
*/

if (isset($_REQUEST['sms_type']))
{
	write_log("[SMSSender] Logging hello paisa sms for reference $_REQUEST[sms_reference] and type $_REQUEST[sms_type]");
	log_hellopaisa_sms(isset($_REQUEST['sms_reference']) ? $_REQUEST['sms_reference'] : null, $_REQUEST['sms_to'], $_REQUEST['sms_type'], $_REQUEST['sms_content']);
}
