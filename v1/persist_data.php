<?php

define('SETTINGS', $_POST['persist_settings']);

include_once 'common.php';

$sql_conn = get_db_connection($_POST['database']);

write_log("[PERSIST_DATA] Persisting data to " . $_POST['database'] . "." . $_POST['table']);

$columns = '';
$values = '';

foreach ($_POST as $key => $val) {
	if ($key == 'persist_settings' || $key == 'database' || $key == 'table') { continue; }
	$columns .= mysqli_real_escape_string($sql_conn, $key) . ',';
	$values .= mysqli_real_escape_string($sql_conn, $val) . ',';
}

$sql = "INSERT INTO " . $_POST['table'] . " (" . substr($columns, 0, strlen($columns) - 1) . ") VALUES (" . substr($values, 0, strlen($values) - 1) . ")";

write_log("[PERSIST_DATA] SQL: $sql");

$insert_result = mysqli_query($sql_conn, $sql);

if (!$insert_result) 
{
	write_log("[PERSIST_DATA] Error inserting into " . $_POST['database'] . "." . $_POST['table']);
	write_log("[PERSIST_DATA] MYSQL ERRNO: " . mysqli_errno($sql_conn));
	write_log("[PERSIST_DATA] MYSQL ERROR: " . mysqli_error($sql_conn));
	sendEmail("Error inserting into " . $_POST['database'] . "." . $_POST['table'],
			  "Error inserting into " . $_POST['database'] . "." . $_POST['table'] . ":\r\n".
			  "MYSQL ERRNO: " . mysqli_errno($sql_conn) . "\r\n" .
			  "MYSQL ERROR: " . mysqli_error($sql_conn) . "\r\n");
}