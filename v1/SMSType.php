<?php

class SMSType {

	const TRANSACTION_CREATED   = 1;
	const PAYMENT_RECEIVED      = 2;
	const PARTNER_PROCESSED     = 3;
	const TRANSACTION_FAILED    = 4;
	const TRANSACTION_REVERSED  = 5;
	const INCOMPLETE_PAYMENT    = 6;
	const CUSTOMER_REGISTRATION = 7;
	const BENEFICIARY_ADDED     = 8;
	const PAYMENT_NOT_REQUIRED  = 9;
	const REGISTER_RECIPIENT    = 10;
	const REFERRAL              = 11;
	const BENEFICIARY_DISABLED  = 12;
	const PAYMENT_CANCELLED     = 13;
	const OTP_FAILED            = 14;
	const OTP_SENDING           = 15;
	const PIN_INCORRECT         = 16;
	const ACCOUNT_BLOCKED       = 17;
	const DEPOSIT_HELLOSTORES   = 18;
	const DEPOSIT_FNB           = 19;
	const VOUCHER_REMINDER      = 20;
	const VOUCHER_EXPIRED       = 21;
	const SCRATCH_CARD          = 22;
	const TRANSACTION_EXPIRY    = 23;
}
