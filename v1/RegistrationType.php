<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RegistrationType
 *
 * @author Tsungai
 */
class RegistrationType
{
	const REGISTRATION	=	1;
	const LOGIN			=	2;
	const VALIDATE_AUTH	=	3;
	const LOGOUT		=	4;
	const CONFIRM_OTP	=	5;
}
