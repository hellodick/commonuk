<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mailer
 *
 * @author Tsungai
 */

define('SETTINGS', $_POST['email_settings']);

include_once 'common.php';

write_log("[MAILER] Received request to send mail\n");

if (!isset($_POST['email_to']) ||
	!isset($_POST['email_subject']) ||
	!isset($_POST['email_content'])) {
	write_log("[MAILER] Invalid data supplied on post\n");
}

$headers = null;

if (!isset($_POST['email_from'])) {
	$headers .= "From: " . $GLOBALS[SETTINGS]['EmailFrom'] . "\r\n" .
			"Reply-To: " . $GLOBALS[SETTINGS]['EmailFrom'] . "\r\n";
        
        if ($_POST['email_attachment'] != null)
        {
            $_POST['email_attachment'] = str_replace(' ', '', $_POST['email_attachment']);
            $attachments = explode(",", $_POST['email_attachment']);
            
            write_log("[MAILER] NUMBER OF ATTACHMENTS = " . count($attachments));
            if(count($attachments) > 1)
            {
                foreach ($attachments as $file)
                {                             
                    $file_size = filesize($file);
                    $handle = fopen($file, "r");
                    $content = fread($handle, $file_size);
                    fclose($handle);
                    $content = chunk_split(base64_encode($content));

                    // a random hash will be necessary to send mixed content
                    $separator = md5(time());

                    // carriage return type (we use a PHP end of line constant)
                    $eol = PHP_EOL;

                    $headers .= "MIME-Version: 1.0" . $eol;
                    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol . $eol;
                    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;

                    // attachment
                    $headers .= "--" . $separator . $eol;
                    $headers .= "Content-Type: application/octet-stream; name=\"" . basename($file) . "\"" . $eol;
                    $headers .= "Content-Transfer-Encoding: base64" . $eol;
                    $headers .= "Content-Disposition: attachment" . $eol . $eol;
                    $headers .= $content . $eol . $eol;
                    $headers .= "--" . $separator . "--";
                }
            }
            else
            {
                    $file = $_POST['email_attachment']; 
                    $file_size = filesize($file);
                    $handle = fopen($file, "r");
                    $content = fread($handle, $file_size);
                    fclose($handle);
                    $content = chunk_split(base64_encode($content));

                    // a random hash will be necessary to send mixed content
                    $separator = md5(time());

                    // carriage return type (we use a PHP end of line constant)
                    $eol = PHP_EOL;

                    $headers .= "MIME-Version: 1.0" . $eol;
                    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol . $eol;
                    $headers .= "Content-Transfer-Encoding: 7bit" . $eol;

                    // attachment
                    $headers .= "--" . $separator . $eol;
                    $headers .= "Content-Type: application/octet-stream; name=\"" . basename($_POST['email_attachment']) . "\"" . $eol;
                    $headers .= "Content-Transfer-Encoding: base64" . $eol;
                    $headers .= "Content-Disposition: attachment" . $eol . $eol;
                    $headers .= $content . $eol . $eol;
                    $headers .= "--" . $separator . "--";
            }
        }
        
	write_log("[MAILER] email_from = " . $GLOBALS[SETTINGS]['EmailFrom'] . "\n");
} else {
	write_log("[MAILER] email_from = " . $_POST['email_from'] . "\n");
}

write_log("[MAILER] email_to = " . $_POST['email_to'] . "\n");
write_log("[MAILER] email_subject = " . $_POST['email_subject'] . "\n");
write_log("[MAILER] email_content = " . $_POST['email_content'] . "\n");

mail($_POST['email_to'],$_POST['email_subject'],$_POST['email_content'],$headers);

write_log("[MAILER] Email sent successfully\n");