<?php

static $ussd_master = null;
static $TSVAS = null;
static $TSEVD = null;
static $PayAT = null;
static $HelloFin = null;
static $Partners = null;

static $log_file = null;

$hp_settings = parse_ini_file("/opt/hellopaisa/hpsettings.ini");
$ussd_settings = parse_ini_file("/opt/hellopaisa/ussd_settings.ini");
$ts_settings = parse_ini_file("/opt/voucher_api/tsservices.ini");
$partner_settings = parse_ini_file("/opt/hellopaisa/partner_settings.ini");
$report_settings = parse_ini_file("/opt/hellopaisa/report_settings.ini");
$sarb_settings = parse_ini_file("/opt/hellopaisa/sarb_settings.ini");
$payat_settings = parse_ini_file("/opt/pay_at/payat_settings.ini");

date_default_timezone_set($GLOBALS[SETTINGS]['Timezone']);