<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TransactionChannel {
    const WEB = 'WEB';
	const POS = 'POS';
    const ADMIN = 'ADMIN';
	const USSD = 'USSD';
    const USSD_ADMIN = 'USSD_ADMIN';
	const SMARTPHONE = 'SMARTPHONE';
    const HELLO_FIN = 'HELLO_FIN';
}