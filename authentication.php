<?php

/**
 * Created by PhpStorm.
 * User: Tsungai
 * Date: 2014/02/17
 * Time: 11:18 AM
 */
if (isset($_SERVER['HTTP_SETTINGS'])) {
    define('SETTINGS', $_SERVER['HTTP_SETTINGS']);
} else {
    define('SETTINGS', 'ts_settings');
}

include_once 'common.php';
include_once 'Response.php';

$dataPOST = trim(file_get_contents('php://input'));

$response_xml = simplexml_load_file($GLOBALS[SETTINGS]['TemplatesLocation'] . "authentication_response.xml");

if (is_null_or_empty("$dataPOST")) {
    write_log("[AUTH] No XML supplied on POST");
    echo get_login_response(ResponseCode::INVALID_DATA_SUPPLIED);
    return;
}

$xml = new SimpleXMLElement($dataPOST);

if ($xml == null) {
    write_log("[AUTH] Invalid XML request specidifed");
    echo get_login_response(ResponseCode::INVALID_DATA_SUPPLIED);
    return;
}

$authentication_request = $xml->xpath('//authentication_request');

if (!$authentication_request) {
    write_log("[AUTH] Invalid authentication_request header specified");
    echo set_response(ResponseCode::INVALID_DATA_SUPPLIED);
    return;
}

$request_type = $xml->xpath('//request_type');

if (!$request_type) {
    write_log("[AUTH] Invalid request type specified");
    echo set_response(ResponseCode::INVALID_DATA_SUPPLIED);
    return;
}


if (!isset($authentication_request[0]->channel)) {
    write_log("[AUTH] Incomplete request. Missing authentication channel.");
    set_response(ResponseCode::INVALID_DATA_SUPPLIED);
    echo formatXMLString($response_xml->asXML());
    return;
}

$channel = $authentication_request[0]->channel;
write_log("[AUTH] Request from channel $channel: " . $request_type[0] . "\n");

if ($request_type[0] == 'validate_auth') {
    echo validate_authentication(
            $authentication_request[0]->auth_user_id, $authentication_request[0]->channel, $authentication_request[0]->auth_token);

    $ar = $response_xml->xpath('//response_code');
    log_authentication($authentication_request[0]->auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, "$ar[0]");
    return;
} else if ($request_type[0] == 'authenticate') {
    echo authenticate_user(
            $authentication_request[0]->username, $authentication_request[0]->password, $authentication_request[0]->channel);
    return;
} else if ($request_type[0] == 'authenticate_qagent') {
    echo authenticate_qagent(
            $authentication_request[0]->msisdn, $authentication_request[0]->pin, $authentication_request[0]->channel);

    $ar = $response_xml->xpath('//response_code');
    log_authentication($authentication_request[0]->auth_user_id, RegistrationType::LOGIN, $channel, "$ar[0]");
    return;
}

function validate_authentication($auth_user_id, $channel, $auth_token) {
    write_log("[AUTH] Validating AuthUser: $auth_user_id with token: $auth_token\n");

    if (!is_valid_auth_user_id("$auth_user_id")) {
        log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::INVALID_DATA_SUPPLIED);
        return get_login_response(ResponseCode::INVALID_DATA_SUPPLIED);
    } else if (!is_valid_auth_token("$auth_token")) {
        log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::INVALID_SESSION);
        return get_login_response_xml(ResponseCode::INVALID_SESSION, "Session expired. Please login again.");
    } else if (!is_valid_channel("$channel")) {
        log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::INVALID_DATA_SUPPLIED);
        return get_login_response(ResponseCode::INVALID_DATA_SUPPLIED);
    }

    $sql_conn = get_db_connection('TSEVD');

    $auth_user_id = mysqli_real_escape_string($sql_conn, $auth_user_id);
    $channel = mysqli_real_escape_string($sql_conn, $channel);

    $auth_result = mysqli_query($sql_conn, "SELECT AuthToken, LastAccess from auth_user"
            . " WHERE AuthUserId = '$auth_user_id'"
            . " AND Channel = '$channel'"
            . " AND Active = 1");

    if (!$auth_result) {
        write_log('Error validating authentication. ERRNO: ' . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error validating authentication", "ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn));
        log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::GENERAL_ERROR);
        return get_login_response_xml(ResponseCode::GENERAL_ERROR, $GLOBALS['general_error']);
    } else if ($auth_result->num_rows != 1) {
        write_log("[AUTH] Existing authentication not found\n");
        log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::INVALID_SESSION);
        return get_login_response_xml(ResponseCode::INVALID_SESSION, "Authentication failed! Session expired.");
    } else {
        $authuser_row = mysqli_fetch_array($auth_result);

        if ($authuser_row['AuthToken'] == $auth_token) {
            $session_time = strtotime($authuser_row['LastAccess']);
            $current_time = time();
            write_log("[AUTH] Session has been active for " . ($current_time - $session_time) . " seconds\n");

            if (($current_time - $session_time) > get_max_auth_time($channel)) {
                write_log("[AUTH] Session time expired!\n");
                log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::SESSION_EXPIRED);
                return get_login_response_xml(ResponseCode::SESSION_EXPIRED, "Session expired.");
            }
        } else {
            write_log("[AUTH] Invalid auth token!\n");
            log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::INVALID_SESSION);
            return get_login_response_xml(ResponseCode::INVALID_SESSION, "Authentication failed! Invalid session.");
        }
    }

    log_authentication($auth_user_id, RegistrationType::VALIDATE_AUTH, $channel, ResponseCode::SUCCESS);

    switch ($channel) {
        /* case 'USSD': {
          //            $q_agent_result = mysqli_query($sql_conn,
          //            "SELECT Msisdn, Pin FROM qagent WHERE QAgentId = '" . get_qagent_id($auth_user_id) . "'");
          if (!is_msisdn_blacklisted($authuser_row['Msisdn'])) {
          //                write_log("[AUTH] Testing password $password == " . $authuser_row['Pin'] . "\n");
          //                if ($authuser_row['Pin'] == $password) {
          //                    return get_success_response_xml($auth_token);
          //                }
          //                else {
          //                    write_log("[AUTH] Invalid password!\n");
          //                    return get_login_response_xml(
          //                        ResponseCode::INVALID_PASSWORD, "Invalid Credentials");
          //                }

          update_last_access_time($auth_user_id, $channel);
          return get_success_response_xml($auth_token);
          }
          else {
          write_log('Msisdn ' . $authuser_row['Msisdn'] . 'is blaklizted!\n');
          return get_login_response_xml(
          ResponseCode::GENERAL_ERROR,
          $GLOBALS['general_error']);
          }
          } */
        case TransactionChannel::USSD_ADMIN: {
                update_last_access_time($auth_user_id);
                return get_success_response_xml($auth_token);
            }
        case TransactionChannel::WEB: {
                update_last_access_time($auth_user_id);
                return get_success_response_xml($auth_token);
            }
        case TransactionChannel::POS: {
                update_last_access_time($auth_user_id);
                return get_success_response_xml($auth_token);
            }
        case TransactionChannel::SMARTPHONE: {
                update_last_access_time($auth_user_id);
                return get_success_response_xml($auth_token);
            }
        case TransactionChannel::ADMIN: {
                update_last_access_time($auth_user_id);
                return get_success_response_xml($auth_token);
            }
        case TransactionChannel::HELLO_FIN: {
                update_last_access_time($auth_user_id);
                return get_success_response_xml($auth_token);
            }
        case TransactionChannel::TREASURY: {
                update_last_access_time($auth_user_id);
                return get_success_response_xml($auth_token);
            }
        default: {
                return get_login_response_xml(ResponseCode::GENERAL_ERROR, $GLOBALS['general_error']);
            }
    }
}

function update_last_access_time($auth_user_id) {

    $sql_conn = get_db_connection('TSEVD');

    $auth_user_id = mysqli_real_escape_string($sql_conn, $auth_user_id);

    $access_result = mysqli_query($sql_conn, "UPDATE auth_user SET LastAccess = sysdate() WHERE AuthUserId = '$auth_user_id'");

    if (!$access_result) {
        write_log("Error updating last access time for $auth_user_id. ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error updating last access time for $auth_user_id", "ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn));
    }
}

function block_user_access($username) {
    $sql_conn = get_db_connection('TSEVD');
    $username = mysqli_real_escape_string($sql_conn, $username);
    $blocked_result = mysqli_query($sql_conn, "Update auth_user set Blocked = 1  WHERE Username = '$username'");
    if (!$blocked_result) {
        write_log("Error updating last blocked for $username. ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error updating blocked for $username", "ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn));
    }
}

function clearFailedLoginAttempts($username) {
    $sql_conn = get_db_connection('TSEVD');
    $username = mysqli_real_escape_string($sql_conn, $username);
    $blocked_result = mysqli_query($sql_conn, "Update auth_user set FailedLoginAttempts = 0 WHERE Username = '$username'");
    if (!$blocked_result) {
        write_log("Error updating last blocked for $username. ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error updating login attempts for $username", "ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn));
    }
}

function increaseNumberOfFailedLoginAttempts($username) {
    $sql_conn = get_db_connection('TSEVD');
    $username = mysqli_real_escape_string($sql_conn, $username);
    $blocked_result = mysqli_query($sql_conn, "Update auth_user set FailedLoginAttempts = FailedLoginAttempts + 1  WHERE Username = '$username'");
    if (!$blocked_result) {
        write_log("Error updating last blocked for $username. ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error updating increasing login attempts for $username", "ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn));
    }
}

function getFailedLoginAttempts($username) {
    $sql_conn = get_db_connection('TSEVD');
    $username = mysqli_real_escape_string($sql_conn, $username);

    if (($blocked_result = mysqli_query($sql_conn, "SELECT FailedLoginAttempts FROM auth_user WHERE Username = '$username'"))) {
        while ($row = mysqli_fetch_assoc($blocked_result)) {
            write_log("[AUTH] Getting number of failed authentication for : $username");
            return $row["FailedLoginAttempts"];
        }
    } else if (!$blocked_result) {
        write_log("Error updating last blocked for $username. ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error getting number of login attempts for $username", "ERRNO: " . mysqli_errno($sql_conn) . " MSG: " . mysqli_error($sql_conn));
    }
}

function authenticate_user($username, $password, $channel) {
    write_log("[AUTH] Authenticating $username on channel $channel\n");

    if (!is_valid_username("$username")) {
        write_log("[AUTH] Invalid username specified: '$username'\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    } else if (!is_valid_password("$password")) {
        write_log("[AUTH] Invalid password specified\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    } else if (!is_valid_channel("$channel")) {
        write_log("[AUTH] Invalid channel specified: '$channel'\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    }

    $sql_conn = get_db_connection('TSEVD');

    $username = mysqli_real_escape_string($sql_conn, $username);
    $channel = mysqli_real_escape_string($sql_conn, $channel);

    if (($q_blocked_user = mysqli_query($sql_conn, "SELECT Blocked FROM auth_user WHERE Username = '$username'"))) {
        while ($row = mysqli_fetch_assoc($q_blocked_user)) {
            if ($row["Blocked"]) {
                write_log("[AUTH] Blocked user : $username, Number of failed login attempts : " . getFailedLoginAttempts($username));
                increaseNumberOfFailedLoginAttempts($username);
                return get_login_response(ResponseCode::BLOCKED);
            }
        }
    }

    $q_agent_result = mysqli_query($sql_conn, "SELECT AuthUserID,QAgentID,Password,AuthToken,DateGenerated"
            . " FROM auth_user WHERE Username = '$username' AND Channel = '$channel' AND Active = 1 AND Blocked = 0");

    if (!$q_agent_result) {
        write_log('Error authenticating user. ERRNO: '
                . mysqli_errno($sql_conn) . " MSG: "
                . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error authenticating user", "ERRNO: " . mysqli_errno($sql_conn)
                . " MSG: " . mysqli_error($sql_conn));
        return get_login_response(ResponseCode::GENERAL_ERROR);
    } else if ($q_agent_result->num_rows != 1) {
        write_log("[AUTH] Username '$username' not registered\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    }

    $authuser_row = mysqli_fetch_array($q_agent_result);

    write_log("[AUTH] Verifying password...\n");
    if ($authuser_row['Password'] == hash("sha512", $password)) {
        //store auth token in table            
        $auth_token = md5(mt_rand());
        write_log("[AUTH] Generated new auth token for AuthUser " . $authuser_row['AuthUserID'] . "\n");
        $result = mysqli_query($sql_conn, "UPDATE auth_user SET AuthToken = '$auth_token',"
                . " DateGenerated = '" . date("Y-m-d H:i:s") . "'"
                . " WHERE AuthUserID = '" . $authuser_row['AuthUserID'] . "'");

        clearFailedLoginAttempts($username);
        if ($result) {
            update_last_access_time($authuser_row['AuthUserID'], $channel);
            log_authentication($authuser_row['AuthUserID'], RegistrationType::LOGIN, $channel, ResponseCode::SUCCESS);
            return get_success_response_xml($auth_token, $authuser_row['DateGenerated'], $GLOBALS[SETTINGS]['Country'], $authuser_row['AuthUserID']);
        } else {
            write_log('Error verifying password ERRNO: '
                    . mysqli_errno($sql_conn) . " MSG: "
                    . mysqli_error($sql_conn) . "\n");
            db_error_log('null', "Error verifying password", "ERRNO: " . mysqli_errno($sql_conn)
                    . " MSG: " . mysqli_error($sql_conn));
            log_authentication($authuser_row['AuthUserID'], RegistrationType::LOGIN, $channel, ResponseCode::GENERAL_ERROR);
            return get_login_response(ResponseCode::GENERAL_ERROR);
        }
    } else {
        $numberOfFailedLoginAttempts = getFailedLoginAttempts($username);
        if ($numberOfFailedLoginAttempts < $GLOBALS[SETTINGS]['MaxPasswordTries']) {
            write_log("[AUTH] Increase number of failed login attempts");
            write_log("[AUTH] Number of failed login attempts : " . $numberOfFailedLoginAttempts);
            increaseNumberOfFailedLoginAttempts($username);
        } else {
            block_user_access($username);
            write_log("[AUTH] Failed to authenticate, Blocking user : $username");
            return get_login_response(ResponseCode::BLOCKED);
        }

        write_log("[AUTH] Invalid password!\n");
        log_authentication($authuser_row['AuthUserID'], RegistrationType::LOGIN, $channel, ResponseCode::INVALID_CREDENTIALS);
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    }
}

function authenticate_qagent($msisdn, $pin, $channel) {
    write_log("[AUTH] Authenticating $msisdn on channel $channel\n");

    if (!is_valid_msisdn("$msisdn")) {
        write_log("[AUTH] Invalid MSISDN specified: '$msisdn'\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    } else if (!is_valid_pin("$pin")) {
        write_log("[AUTH] Invalid pin specified: '$pin'\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    } else if (!is_valid_channel("$channel")) {
        write_log("[AUTH] Invalid channel specified: '$channel'\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    }

    $sql_conn = get_db_connection('TSVAS');

    $msisdn = mysqli_real_escape_string($sql_conn, $msisdn);
    $pin = mysqli_real_escape_string($sql_conn, $pin);
    $channel = mysqli_real_escape_string($sql_conn, $channel);

    $q_agent_result = mysqli_query($sql_conn, "SELECT QAgentId,Pin,Active,Block"
            . " FROM QAgent WHERE Msisdn = '$msisdn'");

    if (!$q_agent_result) {
        write_log('Error authenticating QAgent. ERRNO: '
                . mysqli_errno($sql_conn) . " MSG: "
                . mysqli_error($sql_conn) . "\n");
        db_error_log('null', "Error authenticating QAgent", "ERRNO: " . mysqli_errno($sql_conn)
                . " MSG: " . mysqli_error($sql_conn));
        return get_login_response(ResponseCode::GENERAL_ERROR);
    } else if ($q_agent_result->num_rows != 1) {
        write_log("[AUTH] QAgent with MSISDN '$msisdn' not found\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    }

    $authuser_row = mysqli_fetch_array($q_agent_result);

    write_log("[AUTH] Verifying pin...\n");
    if ($authuser_row['Pin'] != $pin) {
        write_log("[AUTH] Invalid pin specified!\n");
        return get_login_response(ResponseCode::AUTHENTICATION_FAILED);
    } else if ($authuser_row['Active'] != "1") {
        write_log("[AUTH] QAgent not active: " . $authuser_row['QAgentId'] . "\n");
        return get_login_response(ResponseCode::NOT_ACTIVE);
    } else if ($authuser_row['Block'] == "1") {
        write_log("[AUTH] QAgent is blocked: " . $authuser_row['QAgentId'] . "\n");
        return get_login_response(ResponseCode::BLOCKED);
    } else {
        write_log("[AUTH] QAgent authenticated successfully\n");
        return get_login_response(ResponseCode::SUCCESS);
    }
}

function get_max_auth_time($channel) {
    switch ($channel) {
        case TransactionChannel::USSD: return 60 * $GLOBALS[SETTINGS]['SESSION_TIME_USSD'];
        case TransactionChannel::USSD_ADMIN: return 60 * $GLOBALS[SETTINGS]['SESSION_TIME_USSD_ADMIN'];
        case TransactionChannel::WEB: return 60 * $GLOBALS[SETTINGS]['SESSION_TIME_WEB'];
        case TransactionChannel::SMARTPHONE: return 60 * $GLOBALS[SETTINGS]['SESSION_TIME_SMARTPHONE'];
        case TransactionChannel::POS: return 60 * $GLOBALS[SETTINGS]['SESSION_TIME_POS'];
        case TransactionChannel::ADMIN: return 60 * $GLOBALS[SETTINGS]['SESSION_TIME_ADMIN'];
        case TransactionChannel::HELLO_FIN: return 60 * $GLOBALS[SETTINGS]['SESSION_TIME_HELLO_FIN'];
        default: return 0;
    }
}

function get_login_response_xml($response_code, $response_message) {
    $GLOBALS['response_xml']->addChild('response_code', ResponseCode::getStatusCode($response_code));
    $GLOBALS['response_xml']->addChild('response_message', $response_message);
    write_log("[AUTH] Login Response:\n" . formatXMLString($GLOBALS['response_xml']->asXML()));
    return formatXMLString($GLOBALS['response_xml']->asXML());
}

function get_login_response($response_code) {
    return get_login_response_xml($response_code, ResponseCode::getStatusDescription($response_code));
}

function get_success_response_xml($auth_token, $last_login_date = null, $country = 'ZA', $auth_user_id = null) {

	$GLOBALS['response_xml']->addChild('response_code', ResponseCode::getStatusCode(ResponseCode::SUCCESS));
    $GLOBALS['response_xml']->addChild('response_message', ResponseCode::getStatusDescription(ResponseCode::SUCCESS));
    if (!is_null_or_empty($auth_user_id)) {
        $GLOBALS['response_xml']->addChild('auth_user_id', $auth_user_id);
    }
    $GLOBALS['response_xml']->addChild('auth_token', $auth_token);
    if (!is_null_or_empty("$last_login_date")) {
        $GLOBALS['response_xml']->addChild('last_login', $last_login_date);
    }
    if (!is_null_or_empty("$country")) {
        $GLOBALS['response_xml']->addChild('country', $country);
    }
    write_log("[AUTH] Login Response:\n" . formatXMLString($GLOBALS['response_xml']->asXML()));
    return formatXMLString($GLOBALS['response_xml']->asXML());
}

function set_response($response_code) {
    return set_response_xml($response_code, ResponseCode::getStatusDescription($response_code));
}

function set_response_xml($response_code, $response_message) {
    $ar = $GLOBALS['response_xml']->xpath('//result');

    $status = $response_code;
    if (!is_numeric("$response_code")) {
        $status = ResponseCode::getStatusCode($response_code);
    }

    if (!isset($ar[0]->response_code)) {
        write_log("[AUTH] Setting auth response: $response_message\n");
        $ar[0]->addChild('response_code', $status);
        $ar[0]->addChild('response_message', $response_message);
    }

    return formatXMLString($GLOBALS['response_xml']->asXML());
}
