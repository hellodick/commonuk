<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'Response.php';
include_once 'common.php';

function validate_authentication($auth_user_id, $channel, $auth_token)
{
	$request_xml = simplexml_load_file($GLOBALS[SETTINGS]['TemplatesLocation'] . "authentication_request.xml");
	$authentication_request = $request_xml->xpath('//authentication_request');
	$authentication_request[0]->addChild('request_type', 'validate_auth');
	$authentication_request[0]->addChild('auth_user_id', $auth_user_id);
	$authentication_request[0]->addChild('auth_token', $auth_token);
	$authentication_request[0]->addChild('channel', $channel);

	write_log("[AUTH_PROXY] Request XML:\n" . formatXMLString($request_xml->asXML()));
    	
    $ch = curl_init($GLOBALS[SETTINGS]['AuthenticationLocation']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', 'Settings: ' . SETTINGS));
    curl_setopt($ch, CURLOPT_POSTFIELDS, formatXMLString($request_xml->asXML()));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
	
    if (curl_errno($ch)) 
    {
        write_log("[AUTH_PROXY] Auth validation failed ERR: " . curl_errno($ch)
				. " MSG: " . curl_error($ch) . "\n");
        return get_login_response_xml(ResponseCode::GENERAL_ERROR, ResponseCode::getStatusCodeDescription(ResponseCode::GENERAL_ERROR));
    } 
    else 
    {
        curl_close($ch);
        return $output;
    }
}

function authenticate_user($username, $password, $channel)
{
	$request_xml = simplexml_load_file($GLOBALS[SETTINGS]['TemplatesLocation'] . "authentication_request.xml");
	$authentication_request = $request_xml->xpath('//authentication_request');
	$authentication_request[0]->addChild('request_type', 'authenticate');
	$authentication_request[0]->addChild('username', $username);
	$authentication_request[0]->addChild('password', $password);
	$authentication_request[0]->addChild('channel', $channel);

	write_log("[AUTH_PROXY] Request XML:\n" . formatXMLString($request_xml->asXML()));
    	
    $ch = curl_init($GLOBALS[SETTINGS]['AuthenticationLocation']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', 'Settings: ' . SETTINGS));
    curl_setopt($ch, CURLOPT_POSTFIELDS, formatXMLString($request_xml->asXML()));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);

    if (curl_errno($ch)) 
    {
        write_log("[AUTH_PROXY] Authentication failed ERR: " . curl_errno($ch)
				. " MSG: " . curl_error($ch) . "\n");
	    return get_login_response_xml(ResponseCode::GENERAL_ERROR, ResponseCode::getStatusCodeDescription(ResponseCode::GENERAL_ERROR));
    } 
    else 
    {
        curl_close($ch);
        return $output;
    }
}

function authenticate_qagent($msisdn, $pin, $channel)
{
	$request_xml = simplexml_load_file($GLOBALS[SETTINGS]['TemplatesLocation'] . "authentication_request.xml");
	$authentication_request = $request_xml->xpath('//authentication_request');
	$authentication_request[0]->addChild('request_type', 'authenticate_qagent');
	$authentication_request[0]->addChild('msisdn', $msisdn);
	$authentication_request[0]->addChild('pin', $pin);
	$authentication_request[0]->addChild('channel', $channel);

	write_log("[AUTH_PROXY] Request XML:\n" . formatXMLString($request_xml->asXML()));
    	
    $ch = curl_init($GLOBALS[SETTINGS]['AuthenticationLocation']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', 'Settings: ' . SETTINGS));
    curl_setopt($ch, CURLOPT_POSTFIELDS, formatXMLString($request_xml->asXML()));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);

    if (curl_errno($ch)) 
    {
        write_log("[AUTH_PROXY] Authentication failed ERR: " . curl_errno($ch)
				. " MSG: " . curl_error($ch) . "\n");
	    return get_login_response_xml(ResponseCode::GENERAL_ERROR, ResponseCode::getStatusCodeDescription(ResponseCode::GENERAL_ERROR));
    } 
    else 
    {
        curl_close($ch);
        return $output;
    }
}

function get_login_response_xml($response_code, $response_message)
{
    $response_xml = simplexml_load_file($GLOBALS[SETTINGS]['TemplatesLocation'] . "authentication_response.xml");
	$response_xml->addChild('response_code', ResponseCode::getStatusCode($response_code));
	$response_xml->addChild('response_message', $response_message);
    write_log("[AUTH_PROXY] Login Response:\n" . $response_xml->asXML());
    return formatXMLString($response_xml->asXML());
}

function invalidate_session() {
	session_unset();
	session_destroy();
	session_write_close();
	setcookie(session_name(),'',0,'/');
	session_regenerate_id(true);
}

function is_user_logged_in($channel) {

    write_log("[AUTH_PROXY] Validating authentication for user " . $_SESSION['username'] . "\n");
    write_log("[AUTH_PROXY] auth_user_id: " . $_SESSION['auth_user_id'] . "\n");
    write_log("[AUTH_PROXY] session auth_token: " . $_SESSION['auth_token'] . "\n");
	write_log("[AUTH_PROXY] cookie auth_token: " . $_COOKIE['auth_token'] . "\n");
    if (!isset($_SESSION['username']) ||
		!isset($_SESSION['auth_token']) ||
		!isset($_SESSION['auth_user_id'])) {
		write_log("[AUTH_PROXY] No user logged in.\n");
        invalidate_session();
		return false;
    } else if (!isset($_COOKIE['auth_token'])) {
		write_log("[AUTH_PROXY] Invalid auth token.\n");
		invalidate_session();
        return false;
	}
	
	if (!is_valid_auth_token($_COOKIE['auth_token']) ||
		$_COOKIE['auth_token'] != $_SESSION['auth_token']) {
		write_log("[AUTH_PROXY] COOKIE['auth_token'] " . $_COOKIE['auth_token'] . " != SESSION['auth_token'] " . $_SESSION['auth_token'] . "\n");
		invalidate_session();
		return false;
	}
	
    $auth_response = simplexml_load_string(validate_authentication($_SESSION['auth_user_id'], $channel, $_SESSION['auth_token']));
    $lr = $auth_response->xpath('//authentication_response');
    
    write_log("[AUTH_PROXY] Auth validation response message: " . $lr[0]->response_message . "\n");

    if (($lr[0]->response_code == ResponseCode::getStatusCode(ResponseCode::SUCCESS))) {
        return true;
    }
	
	invalidate_session();
    return false;
}
