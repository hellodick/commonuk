<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tsungai
 * Date: 2015/03/04
 * Time: 03:25 PM
 */


$PaymentProcessorErrorMsg['00'] = 'Successful';
$PaymentProcessorErrorMsg['08'] = 'Account reference not valid for this Bill Issuer.';
$PaymentProcessorErrorMsg['09'] = 'Payment not allowed: Call Bill Issuer';
$PaymentProcessorErrorMsg['32'] = 'Login failed';
$PaymentProcessorErrorMsg['35'] = 'Transaction ID invalid';
$PaymentProcessorErrorMsg['47'] = 'General Error';
$PaymentProcessorErrorMsg['51'] = 'Invalid Amount';
$PaymentProcessorErrorMsg['62'] = 'Confirmation Failed. Transaction already confirmed';
$PaymentProcessorErrorMsg['72'] = 'Void Failed. Transaction already cancelled';
$PaymentProcessorErrorMsg['81'] = 'Void Failed. Transaction has been confirmed';

$PaymentProcessorErrorMsg['100'] = 'Invalid login credentials';
$PaymentProcessorErrorMsg['101'] = 'No account number supplied';
$PaymentProcessorErrorMsg['102'] = 'No amount supplied';
$PaymentProcessorErrorMsg['103'] = 'No store ID supplied';
$PaymentProcessorErrorMsg['104'] = 'No till ID supplied';
$PaymentProcessorErrorMsg['105'] = 'No retailer supplied';

$PaymentProcessorErrorMsg['106'] = 'Invalid realtime';
$PaymentProcessorErrorMsg['107'] = 'Invalid verifyonly';

$PaymentProcessorErrorMsg['108'] = 'No PaymentRefNumber supplied';
$PaymentProcessorErrorMsg['109'] = 'No PaymentReceiptNo supplied';

$PaymentProcessorErrorMsg['110'] = 'No TransactionID supplied';
$PaymentProcessorErrorMsg['111'] = 'No TenderType supplied';
$PaymentProcessorErrorMsg['112'] = 'Invalid ConfirmationType';

class PaymentProcessorResponse {
	
	static function getVoucherAPIResponse($errorCode)
	{
		switch ($errorCode)
		{
			case '00':  return '0';
			case '08':
			case '09':
			case '32':
			case '35':
			case '47':
			case '51':
			case '62':
			case '72':
			case '81':
			case '100':
			case '101':
			case '102':
			case '103':
			case '104':
			case '105':
			case '106':
			case '107':
			case '108':
			case '109':
			case '110':
			case '111':
			case '112': return $errorCode;
		}
	}
}