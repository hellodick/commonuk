<?php

include_once 'common.php';

Class Mutex {
	public static function acquire($id, $path='/tmp/') {
		
		$lockFile = self::__generateLockFileName($id, $path);

		if(self::__lockExists($lockFile)) { return false; }

		return self::__createLock($lockFile);

	}

	public static function release($id, $path='/tmp/') {

		$lockFile = self::__GenerateLockFileName($id, $path);
		write_log("[MUTEX] Releasing lock $lockFile");

		if (self::__lockExists($lockFile)) { return unlink($lockFile); }

		return true;
	}

	public static function refresh($id, $path='/tmp/') {
		return touch(self::__generateLockFileName($id, $path));
	}

	private static function __createLock($lockFile){

		write_log("[MUTEX] Attempting to create lock file $lockFile");
		if (!$fp = fopen($lockFile, 'w')) { return false; }

		fwrite($fp, "Mutex lock file - DO NOT DELETE");
		fclose($fp);

		touch($lockFile);

		return true;
	}	

	private static function __lockExists($lockFile) {
		return file_exists($lockFile);
	}

	private static function __generateLockFileName($id, $path) {
		return rtrim($path, '/') . '/' . $id . '.lock';
	}
}
