<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Response
 *
 * @author Tsungai
 */
$general_error = "A general error occurred on the system. Try again later.";

$response_codes = array();

class ResponseCode {

    const GENERAL_ERROR = 'GENERAL_ERROR';
    const SUCCESS = 'SUCCESS';
    //authentication response codes
    const INVALID_PASSWORD = 'INVALID_PASSWORD';
    const INVALID_SESSION = 'INVALID_SESSION';
    const SESSION_EXPIRED = 'SESSION_EXPIRED';
    const USER_NOT_REGISTERED = 'USER_NOT_REGISTERED';
    const AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED';
    const NOT_ACTIVE = 'NOT_ACTIVE';
    const BLOCKED = 'BLOCKED';
    const INVALID_OTP = 'INVALID_OTP';
    //HelloPaisa response codes
    const ACTIVE = 'Active';
    const PENDING = 'Pending';
    const INVALID_REQUEST = 'INVALID_REQUEST';
    const INVALID_CREDENTIALS = 'INVALID_CREDENTIALS';
    const INVALID_ACCOUNT = 'INVALID_ACCOUNT';
    const TRANSACTION_NOT_FOUND = 'TRANSACTION_NOT_FOUND';
    const MATCHED = 'MATCHED';
    const UNMATCHED = 'UNMATCHED';
    const PARTNER_SENT = 'PARTNER_SENT';
    const PARTNER_UNSENT = 'PARTNER_UNSENT';
    const PARTNER_RETRY = 'PARTNER_RETRY';
    const PARTNER_CONFIRMED = 'PARTNER_CONFIRMED';
    const PARTNER_FAILED = 'PARTNER_FAILED';
    const PARTNER_REVERSED = 'PARTNER_REVERSED';
    //general response codes
    const INVALID_DATA_SUPPLIED = 'INVALID_DATA_SUPPLIED';
    const DUPLICATE_USER = 'DUPLICATE_USER';
    const NOT_SUPPORTED = 'NOT_SUPPORTED';
    //voucher purchase response codes
    const INSUFFICIENT_STOCK = 'INSUFFICIENT_STOCK';
    const INSUFFICIENT_FUNDS = 'INSUFFICIENT_FUNDS';
    const PERFORMED_REVERSAL = 'PERFORMED_REVERSAL';
    const NON_EXISTENT = 'NON_EXISTENT';
    const NOT_COMPLETELY_SUCCESSFUL = 'NOT_COMPLETELY_SUCCESSFUL';

    public static function getStatusDescription($response_code) {
        $sql_conn = get_db_connection('TSEVD');

        $status_code_result = mysqli_query($sql_conn, "SELECT Description FROM va_status_codes"
                . " WHERE Status = '$response_code'");

        if (!$status_code_result) {
            write_log("Error getting response code description"
                    . " for response code $response_code"
                    . ' ERRNO: ' . mysqli_errno($sql_conn)
                    . ' MSG: ' . mysqli_error($sql_conn) . "\n");
            db_error_log('null', "Error getting response code description"
                    . " for response code $response_code", "ERRNO: " . mysqli_errno($sql_conn)
                    . " MSG: " . mysqli_error($sql_conn));
            return $GLOBALS['general_error'];
        } else if ($status_code_result->num_rows == 1) {
            $desc = mysqli_fetch_row($status_code_result);
            return $desc[0];
        }
    }

    public static function getStatusCodeDescription($response_code) {
        $sql_conn = get_db_connection('TSEVD');

        if (!is_numeric("$response_code")) {
            return $GLOBALS['general_error'];
        }

        $status_code_result = mysqli_query($sql_conn, "SELECT Description FROM va_status_codes"
                . " WHERE StatusCode = '$response_code'");

        if (!$status_code_result) {
            write_log("Error getting response code description"
                    . " for response code $response_code"
                    . ' ERRNO: ' . mysqli_errno($sql_conn)
                    . ' MSG: ' . mysqli_error($sql_conn) . "\n");
            db_error_log('null', "Error getting response code description"
                    . " for response code $response_code", "ERRNO: " . mysqli_errno($sql_conn)
                    . " MSG: " . mysqli_error($sql_conn));
            return $GLOBALS['general_error'];
        } else if ($status_code_result->num_rows == 1) {
            $desc = mysqli_fetch_row($status_code_result);
            return $desc[0];
        }
    }

    public static function getStatusCode($response_code) {

        if (is_numeric("$response_code")) {
            return $response_code;
        }

        $sql_conn = get_db_connection('TSEVD');

        $status_code_result = mysqli_query($sql_conn, "SELECT StatusCode FROM va_status_codes"
                . " WHERE Status = '$response_code'");

        if (!$status_code_result) {
            write_log("Error getting status code for response code $response_code"
                    . ' ERRNO: ' . mysqli_errno($sql_conn)
                    . ' MSG: ' . mysqli_error($sql_conn) . "\n");
            db_error_log('null', "Error getting status code for response code $response_code", "ERRNO: " . mysqli_errno($sql_conn)
                    . " MSG: " . mysqli_error($sql_conn));
            return $GLOBALS['general_error'];
        } else if ($status_code_result->num_rows == 1) {
            $desc = mysqli_fetch_row($status_code_result);
            return $desc[0];
        }
    }

}

class HFResponseCode {

    public static function getStatusDescription($response_code) {
        $sql_conn = get_db_connection('HelloFin');

        $status_code_result = mysqli_query($sql_conn, "SELECT Comment FROM HelloStatus WHERE Description = '$response_code'");

        if (!$status_code_result) {
            write_log("[Response] Error getting response code description for response code $response_code");
            write_log('[Response] ERRNO: ' . mysqli_errno($sql_conn));
            write_log('[Response] MSG: ' . mysqli_error($sql_conn));
            return $GLOBALS['general_error'];
        } else if ($status_code_result->num_rows == 1) {
            $desc = mysqli_fetch_row($status_code_result);
            return $desc[0];
        }
    }

    public static function getStatusCodeDescription($response_code) {
        $sql_conn = get_db_connection('HelloFin');

        if (!is_numeric("$response_code")) {
            return $GLOBALS['general_error'];
        }

        $status_code_result = mysqli_query($sql_conn, "SELECT Comment FROM HelloStatus WHERE Id = '$response_code'");

        if (!$status_code_result) {
            write_log("[Response] Error getting response code description");
            write_log('[Response] ERRNO: ' . mysqli_errno($sql_conn));
            write_log('[Response] MSG: ' . mysqli_error($sql_conn));
            return $GLOBALS['general_error'];
        } else if ($status_code_result->num_rows == 1) {
            $desc = mysqli_fetch_row($status_code_result);
            return $desc[0];
        }
    }

    public static function getStatusCode($response_code) {
        if (is_numeric("$response_code")) {
            return $response_code;
        }

        $sql_conn = get_db_connection('HelloFin');

        $status_code_result = mysqli_query($sql_conn, "SELECT Id FROM HelloStatus WHERE Description = '$response_code'");

        if (!$status_code_result) {
            write_log("[Response] Error getting status code for response code $response_code");
            write_log('[Response] ERRNO: ' . mysqli_errno($sql_conn));
            write_log('[Response] MSG: ' . mysqli_error($sql_conn));
            return $GLOBALS['general_error'];
        } else if ($status_code_result->num_rows == 1) {
            $desc = mysqli_fetch_row($status_code_result);
            return $desc[0];
        }
    }

}

class WMResponseCode {

    public static function getDescription($response_code) {
        $sql_conn = get_db_connection('TSVAS');

        $status_code_result = mysqli_query($sql_conn, "SELECT reason FROM wm_status_codes"
                . " WHERE statusCode = '$response_code'");

        if (!$status_code_result) {
            write_log("Error getting wm description for response code $response_code."
                    . ' ERRNO: ' . mysqli_errno($sql_conn)
                    . ' MSG: ' . mysqli_error($sql_conn) . "\n");
            db_error_log('null', "Error getting wm description for response code $response_code", "ERRNO: " . mysqli_errno($sql_conn)
                    . " MSG: " . mysqli_error($sql_conn));
            return $GLOBALS['general_error'];
        } else if ($status_code_result->num_rows == 1) {
            $desc = mysqli_fetch_row($status_code_result);
            return $desc[0];
        }
    }

}
